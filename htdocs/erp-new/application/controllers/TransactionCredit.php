<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TransactionCredit extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Transaction_model');
		$this->load->model('Stock_card_model');
	}
	public function form()
	{
		$q_No = $this->Db_umum->row("SELECT transaction_id FROM transaction");
		$data['transaction_number'] = 'PJK-'.$q_No;
		$data['data_member'] = $this->Db_umum->getSelect("member_id, member_number, member_name, member_credit","member")->result_array();
		$data['data_item'] = $this->Db_umum->getSelect("item_id, item_number, item_name, item_sold_price, item_stock","item")->result_array();

		show("transaction/credit_form", "Input Penjualan Kredit", $data);
	}
	public function list()
	{
		$data['data_array'] = $this->Transaction_model->getAllCredit();

		show("transaction/credit_list", "Riwayat Penjualan Kredit", $data);
	}
	public function listRecieve()
	{
		$data['data_array'] = $this->Transaction_model->getAllCreditReceive();

		show("transaction/receive_list", "Penerimaan Piutang", $data);
	}
	function save()
	{
		$data = array(
			"member_id" => $this->input->post("member_id"),
			"transaction_number" => $this->input->post("transaction_number"),
			"transaction_date" => $this->input->post("transaction_date"),
			"transaction_due_date" => $this->input->post("transaction_due_date"),
			"transaction_total" => $this->input->post("transaction_total"),
			"transaction_discount" => $this->input->post("transaction_discount"),
			"transaction_grand_total" => $this->input->post("transaction_grand_total"),
			"transaction_down_payment" => preg_replace("/\D/", '',$this->input->post("transaction_down_payment")),
			"transaction_type" => 2,
			"transaction_create_date" => date("Y-m-d H:i:s"),
			"transaction_state" => 1,
			);
		$transaction_id = $this->Db_umum->insert("transaction",$data);
		//update credit member
		$member = $this->Db_umum->getById("member_credit","member","member_id",$this->input->post("member_id"))->row_array();
		$final_credit = $member['member_credit'] + ($this->input->post("transaction_grand_total") - preg_replace("/\D/", '',$this->input->post("transaction_down_payment")));
		$data = array(
			"member_credit" => $final_credit,
			);
		$this->Db_umum->update("member","member_id",$this->input->post("member_id"),$data);
		//add receive DP
		$data = array(
			"transaction_id" => $transaction_id,
			"recieve_amount" => preg_replace("/\D/", '',$this->input->post("transaction_down_payment")),
			"recieve_type" => 1,
			"recieve_create_date" => date("Y-m-d H:i:s"),
			"recieve_state" => 1,
			);
		$this->Db_umum->insert("transaction_credit_recieve",$data);

		$data_piutang = array(
			"transaction_id" => $transaction_id,
			"date" => $this->input->post("transaction_date"),
			"member_id" => $this->input->post("member_id"),
			"in" => $this->input->post("transaction_grand_total"),
			"out" => 0,
			"description" => 'Penjualan '.$this->input->post("transaction_number"),
			"saldo" => $member['member_credit'] + $this->input->post("transaction_grand_total"),
			);
		$this->Db_umum->insert("kartu_piutang", $data_piutang);

		$data_piutang2 = array(
			"transaction_id" => $transaction_id,
			"date" => $this->input->post("transaction_date"),
			"member_id" => $this->input->post("member_id"),
			"in" => 0,
			"out" => preg_replace("/\D/", '',$this->input->post("transaction_down_payment")),
			"description" => 'DP '.$this->input->post("transaction_number"),
			"saldo" => $final_credit,
			);
		$this->Db_umum->insert("kartu_piutang", $data_piutang2);

		$detail_id = $this->input->post("item_id",TRUE);
		
		$count =  count($detail_id);
		for($i=0; $i<$count; $i++){
			$data = array(
				"transaction_id" => $transaction_id,
				"item_id" => $_POST['item_id'][$i],
				"detail_qty" => $_POST['detail_qty'][$i],
				"detail_price" => $_POST['detail_price'][$i],
				);
			$this->Db_umum->insert("transaction_detail",$data);

			$item = $this->Db_umum->getById("item_stock","item","item_id",$_POST['item_id'][$i])->row_array();
			$final_stock = $item['item_stock'] - $_POST['detail_qty'][$i];
			$data = array(
				"item_stock" => $final_stock,
				);
			$this->Db_umum->update("item","item_id",$_POST['item_id'][$i],$data);

			$last_mutation = $this->Stock_card_model->getlastRecord($_POST['item_id'][$i]);
			$stock_total = $_POST['detail_price'][$i] * $_POST['detail_qty'][$i]; 
			$balance_stock_qty = $last_mutation['balance_stock_qty'] - $_POST['detail_qty'][$i];
			$balance_stock_price = $_POST['detail_price'][$i];
			$balance_stock_total = $last_mutation['balance_stock_total'] - $stock_total;
			$data2 = array(
				"item_id" => $_POST['item_id'][$i],
				"id_order" => $this->input->post("transaction_number"),
				"stock_qty" => $_POST['detail_qty'][$i],
				"stock_price" => $_POST['detail_price'][$i],
				"stock_total" => $stock_total,
				"balance_stock_qty" => $balance_stock_qty,
				"balance_stock_price" => $balance_stock_price,
				"balance_stock_total" => $balance_stock_total,
				"description" => "Penjualan Cash",
				"stock_type" => 3,
				"date" =>date("Y-m-d H:i:s"),
				);
			$this->Db_umum->insert("stok_mutasi",$data2);

		}
		$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Input Pembelian berhasil</div>');
		redirect('panel/transaction/credit/formCredit','refresh');
	}
	function saveRecieve()
	{
		$data = array(
			"transaction_id" => $this->input->post("id"),
			"recieve_amount" => preg_replace("/\D/", '',$this->input->post("recieve_amount")),
			"recieve_type" => 2,
			"recieve_create_date" => date("Y-m-d H:i:s"),
			"recieve_state" => 1,
			);
		$this->Db_umum->insert("transaction_credit_recieve",$data);
		$member = $this->Db_umum->select_row2("SELECT member_credit FROM member WHERE member_id = '".$_POST['id_member']."' ");
		$data = array(
			"member_credit" => $member->member_credit - preg_replace("/\D/", '',$this->input->post("recieve_amount")),
			);
		$this->Db_umum->update("member","member_id",$_POST['id_member'],$data);

		$data_piutang = array(
			"transaction_id" => $this->input->post("id"),
			"date" => date("Y-m-d H:i:s"),
			"member_id" => $_POST['id_member'],
			"in" => 0,
			"out" => preg_replace("/\D/", '',$this->input->post("recieve_amount")),
			"description" => 'Pembayaran Piutang',
			"saldo" => $member->member_credit - preg_replace("/\D/", '',$this->input->post("recieve_amount")),
			);
		$this->Db_umum->insert("kartu_piutang", $data_piutang);

		$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Penerimaan piutang kredit berhasil</div>');
		redirect('panel/receivable/listReceivable','refresh');
	}
	function detail($id)
	{
		$array_data = $this->Transaction_model->getDetailCredit($id);
		$jason['detail_transaction'] = array();
		$number = 0;
		foreach ($array_data as $data) {
			$item['number'] = ++$number;
			$item['name'] = $data['item_name'];
			$item['qty'] = $data['detail_qty'];
			$item['price'] = $data['detail_price'];
			$item['sub_total'] = $data['sub_total'];
			array_push($jason['detail_transaction'],$item);
		}
		echo json_encode($jason);
	}
	function detailReceive($id)
	{
		$array_data = $this->Transaction_model->getDetailCreditReceive($id);

		$jason["detail_recieve"] = array();
		$i = 0; $total_recieve = 0;
		foreach ($array_data as $data) {
			$total_recieve += $data["recieve_amount"];
			$receive["sort"] = "Ke-".++$i; 
			$receive["date"] = indonesianDate($data['recieve_create_date']);
			$receive["pay"] = "Rp ".number_format($data["recieve_amount"],0, ",","."); 
			array_push($jason['detail_recieve'], $receive);                            
		}
		$jason["total"] = "Rp ".number_format($total_recieve,0, ",",".");
		echo json_encode($jason, JSON_PRETTY_PRINT);  
	}

	function receivableCard()
	{
		if(isset($_POST['member_id']))
		{
			$start_date = $_POST['start_date'];
			$end_date = $_POST['end_date'];
			$member_id = $_POST['member_id'];
		}else
		{
			$start_date = '';
			$end_date = '';
			$member_id = '';
		}

		$data['member'] = $this->db->query("SELECT member_id,member_name FROM member")->result_array();
		$data['data_array'] = $this->db->query("SELECT * FROM kartu_piutang WHERE member_id = '".$member_id."' AND (date BETWEEN '".$start_date."' AND '".$end_date."')")->result_array();

		show("receivable_card/index", "Kartu Piutang", $data);
	}
}

/* End of file TransactionCredit.php */
/* Location: ./application/controllers/TransactionCredit.php */