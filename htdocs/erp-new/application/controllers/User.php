<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Encrypter');
	}

	public function index()
	{
		$this->db->select("id, username, jenis, password");
		$this->db->from("user");
		$this->db->where("username !=", "admin");
		$data['data_array'] = ($this->db->get())->result_array();

		show("user/user_list", "User", $data);
	}

	public function save()
	{
		$id = $this->input->post("id");
		if($id == 0){
			$data = array(
				"username" => $this->input->post("username"),
				"password" =>  $this->encrypter->encryptIt($this->input->post("password2")),
				"jenis" => $this->input->post("jenis"),
			);
			$this->Db_umum->insert("user",$data);
			$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Input data berhasil</div>');
		} else {
			$username = $this->input->post("username");
			if($this->input->post("password2") == "")
			{
				$password = $this->input->post("password");
			}else{
				$password = $this->encrypter->encryptIt($this->input->post("password2"));
			}
			$jenis = $this->input->post("jenis");
			$data = array(
				"username" => $username,
				"password" => $password,
				"jenis" => $jenis,
			);
			$this->Db_umum->update("user","id",$id,$data);
			$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Edit data berhasil</div>');
		}
		redirect('panel/user/listUser','refresh');
	}

	function delete($id)
	{
		$this->Db_umum->delete("user","id", $id);
		$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Hapus data berhasil</div>');
		redirect('panel/user/listUser','refresh');
	}
}
