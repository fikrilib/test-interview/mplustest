<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data['data_array'] = $this->Db_umum->getSelect("member_id, member_number, member_name, member_address, member_phone","member")->result_array();

		show("member/member_list", "Anggota", $data);
	}
	function save()
	{
		$id = $this->input->post("id");
		if($id == 0){
			$data = array(
				"member_number" => $this->input->post("member_number"),
				"member_name" => $this->input->post("member_name"),
				"member_address" => $this->input->post("member_address"),
				"member_phone" => $this->input->post("member_phone"),
				"member_credit" => 0,
				"member_create_date" => date("Y-m-d H:i:s"),
				"member_state" => 1,
			);
			$this->Db_umum->insert("member",$data);
			$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Input data berhasil</div>');
		} else {
			$data = array(
				"member_number" => $this->input->post("member_number"),
				"member_name" => $this->input->post("member_name"),
				"member_address" => $this->input->post("member_address"),
				"member_phone" => $this->input->post("member_phone"),
				"member_last_update" => date("Y-m-d H:i:s")
			);
			$this->Db_umum->update("member","member_id",$id,$data);
			$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Edit data berhasil</div>');
		}
		redirect('panel/member/listMember','refresh');
	}
	function delete($id)
	{
		$this->Db_umum->delete("member","member_id", $id);
		$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Hapus data berhasil</div>');
		redirect('panel/member/listMember','refresh');
	}

}

/* End of file Member.php */
/* Location: ./application/controllers/Member.php */