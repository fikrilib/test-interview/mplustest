<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function index()
	{
		$view = "dashboard/dashboard";
		show($view, "Dashboard", NULL);
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */