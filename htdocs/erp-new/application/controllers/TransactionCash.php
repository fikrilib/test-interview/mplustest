<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TransactionCash extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Transaction_model');
		$this->load->model('Stock_card_model');
	}
	public function form()
	{
		$q_No = $this->Db_umum->row("SELECT transaction_id FROM transaction");
		$data['transaction_number'] = 'PJ-'.$q_No;
		$data['data_member'] = $this->Db_umum->getSelect("member_id, member_number, member_name","member")->result_array();
		$data['data_item'] = $this->Db_umum->getSelect("item_id, item_number, item_name, item_sold_price, item_stock","item")->result_array();

		show("transaction/cash_form", "Input Penjualan Tunai", $data);
	}
	public function list()
	{
		$data['data_array'] = $this->Transaction_model->getAllCash();

		show("transaction/cash_list", "Riwayat Penjualan Tunai", $data);
	}
	function save()
	{
		$data = array(
			"member_id" => $this->input->post("member_id"),
			"transaction_number" => $this->input->post("transaction_number"),
			"transaction_date" => $this->input->post("transaction_date"),
			"transaction_total" => $this->input->post("transaction_total"),
			"transaction_discount" => $this->input->post("transaction_discount"),
			"transaction_grand_total" => $this->input->post("transaction_grand_total"),
			"transaction_down_payment" => 0,
			"transaction_type" => 1,
			"transaction_create_date" => date("Y-m-d H:i:s"),
			"transaction_state" => 1,
			);
		$transaction_id = $this->Db_umum->insert("transaction",$data);

		$detail_id = $this->input->post("item_id",TRUE);
		
		$count =  count($detail_id);
		for($i=0; $i<$count; $i++){
			$data = array(
				"transaction_id" => $transaction_id,
				"item_id" => $_POST['item_id'][$i],
				"detail_qty" => $_POST['detail_qty'][$i],
				"detail_price" => $_POST['detail_price'][$i],
				);
			$this->Db_umum->insert("transaction_detail",$data);

			$item = $this->Db_umum->getById("item_stock","item","item_id",$_POST['item_id'][$i])->row_array();
			$final_stock = $item['item_stock'] - $_POST['detail_qty'][$i];
			$data = array(
				"item_stock" => $final_stock,
				);
			$this->Db_umum->update("item","item_id",$_POST['item_id'][$i],$data);

			$last_mutation = $this->Stock_card_model->getlastRecord($_POST['item_id'][$i]);
			$stock_total = $_POST['detail_price'][$i] * $_POST['detail_qty'][$i]; 
			$balance_stock_qty = $last_mutation['balance_stock_qty'] - $_POST['detail_qty'][$i];
			$balance_stock_price = $_POST['detail_price'][$i];
			$balance_stock_total = $last_mutation['balance_stock_total'] - $stock_total;
			$data2 = array(
				"item_id" => $_POST['item_id'][$i],
				"id_order" => $this->input->post("transaction_number"),
				"stock_qty" => $_POST['detail_qty'][$i],
				"stock_price" => $_POST['detail_price'][$i],
				"stock_total" => $stock_total,
				"balance_stock_qty" => $balance_stock_qty,
				"balance_stock_price" => $balance_stock_price,
				"balance_stock_total" => $balance_stock_total,
				"description" => "Penjualan Cash",
				"stock_type" => 3,
				"date" =>date("Y-m-d H:i:s"),
				);
			$this->Db_umum->insert("stok_mutasi",$data2);

		}
		$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Input Pembelian berhasil</div>');
		redirect('panel/transaction/cash/formCash','refresh');
	}
	function detail($id)
	{
		$array_data = $this->Transaction_model->getDetailCash($id);
		$jason['detail_transaction'] = array();
		$number = 0;
		foreach ($array_data as $data) {
			$item['number'] = ++$number;
			$item['name'] = $data['item_name'];
			$item['qty'] = $data['detail_qty'];
			$item['price'] = $data['detail_price'];
			$item['sub_total'] = $data['sub_total'];
			array_push($jason['detail_transaction'],$item);
		}
		echo json_encode($jason);
	}

	function barcodeForm()
	{
		$data['data_array'] = $this->Transaction_model->getAllCash();
		$data['data_item'] = $this->Db_umum->getSelect("item_id, item_number, item_name, item_sold_price, item_stock","item")->result_array();
		$data['data_member'] = $this->Db_umum->getSelect("member_id, member_number, member_name","member")->result_array();
		show("transaction/barcode", "Input Penjualan Tunai", $data);
	}

	function barcode()
	{
		$kode = $_POST['barcode'];
		$cek = $this->Db_umum->row("SELECT item_id FROM item WHERE item_number = '".$kode."' ");
		if($cek > 0)
		{
			$q = $this->Db_umum->select_row("SELECT item_id,item_number,item_name,item_sold_price FROM item WHERE item_number = '".$kode."' ");
			$hasil['kode'] = $kode;
			$hasil['nama'] = $q->item_name;
			$hasil['harga_jual'] = number_format($q->item_sold_price);
			$hasil['harga'] = $q->item_sold_price;
			$hasil['id_barang'] = $q->item_id;
			$hasil['hasil'] = 'sukses';
		}else
		{
			$hasil['hasil'] = 'gagal';
		}
		echo json_encode($hasil);
	}

	function selesai()
	{
		$q_No = $this->Db_umum->row("SELECT transaction_id FROM transaction");
		$transaction_number = 'PJ-'.$q_No;
		$member_id = $this->input->post("member_id");
		$transaction_date = date('Y-m-d');
		$transaction_discount = $this->input->post('diskon_seluruh');
		if ($transaction_discount == '' || $transaction_discount == null) {
			$transaction_discount = 0;
		}
		$transaction_grand_total = $this->input->post('total_2');
		$transaction_total = $this->input->post('total_thok');
		$transaction_down_payment = 0;
		$transaction_type = 1;
		$transaction_create_date = date("Y-m-d H:i:s");
		$transaction_state = 1;

		$data = array(
			"transaction_number" => $transaction_number,
			"member_id" => $member_id,
			"transaction_date" => $transaction_date,
			"transaction_total" => $transaction_total,
			"transaction_discount" => $transaction_discount,
			"transaction_grand_total" => $transaction_grand_total,
			"transaction_down_payment" => $transaction_down_payment,
			"transaction_type" => $transaction_type,
			"transaction_create_date" => $transaction_create_date,
			"transaction_state" => $transaction_state,
			);

		$transaction_id = $this->Db_umum->insert("transaction",$data);

		$detail_id = $this->input->post("kd_barang",TRUE);
		
		$count =  count($detail_id);
		for($i=0; $i<$count; $i++){
			$data = array(
				"transaction_id" => $transaction_id,
				"item_id" => $_POST['id_barang'][$i],
				"detail_qty" => $_POST['qty'][$i],
				"detail_price" => $_POST['price'][$i],
				);
			$this->Db_umum->insert("transaction_detail",$data);

			$item = $this->Db_umum->getById("item_stock","item","item_id",$_POST['id_barang'][$i])->row_array();
			$final_stock = $item['item_stock'] - $_POST['qty'][$i];
			$data = array(
				"item_stock" => $final_stock,
				);
			$this->Db_umum->update("item","item_id",$_POST['id_barang'][$i],$data);

			$last_mutation = $this->Stock_card_model->getlastRecord($_POST['id_barang'][$i]);
			$stock_total = $_POST['price'][$i] * $_POST['qty'][$i]; 
			$balance_stock_qty = $last_mutation['balance_stock_qty'] - $_POST['qty'][$i];
			$balance_stock_price = $_POST['price'][$i];
			$balance_stock_total = $last_mutation['balance_stock_total'] - $stock_total;
			$data2 = array(
				"item_id" => $_POST['id_barang'][$i],
				"id_order" => $transaction_number,
				"stock_qty" => $_POST['qty'][$i],
				"stock_price" => $balance_stock_price,
				"stock_total" => $stock_total,
				"balance_stock_qty" => $balance_stock_qty,
				"balance_stock_price" => $balance_stock_price,
				"balance_stock_total" => $balance_stock_total,
				"description" => "Penjualan Barcode",
				"stock_type" => 3,
				"date" =>date("Y-m-d H:i:s"),
				);
			$this->Db_umum->insert("stok_mutasi",$data2);
		}
		$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Input Pembelian berhasil</div>');
		redirect('panel/transaction/cash/barcode','refresh');
	}

	function daftarkeranjang()
	{

	}

	function total()
	{

	}

	function faktur()
	{

	}

}

/* End of file TransactionCash.php */
/* Location: ./application/controllers/TransactionCash.php */