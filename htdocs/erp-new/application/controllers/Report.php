<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Purchasing_model');
		$this->load->model('Transaction_model');
		$this->load->model('Item_model');
	}

	public function transaction()
	{
		if(isset($_POST['tahun']) && isset($_POST['bulan']))
		{
			$month = $_POST['bulan'];
			$year = $_POST['tahun'];
			$data['bulanne'] = $_POST['bulan'];
			$data['tahunne'] = $_POST['tahun'];
		}else{
			$month = date('m');
			$year = date('Y');
			$data['bulanne'] = date('m');
			$data['tahunne'] = date('Y');
		}

		$data['data_array'] = $this->db->query("SELECT a.purchasing_id, a.purchasing_number, a.purchasing_date, a.purchasing_total, a.purchasing_discount, a.purchasing_grand_total, b.member_name, c.supplier_name FROM purchasing a INNER JOIN member b ON a.member_id = b.member_id INNER JOIN supplier c ON a.supplier_id = c.supplier_id WHERE MONTH(a.purchasing_date) = '".$month."' AND YEAR(a.purchasing_date) = '".$year."' ")->result_array();

		show("report/purchasing", "Riwayat Pembelian", $data);
	}

	public function cash()
	{
		if(isset($_POST['tahun']) && isset($_POST['bulan']))
		{
			$month = $_POST['bulan'];
			$year = $_POST['tahun'];
			$data['bulanne'] = $_POST['bulan'];
			$data['tahunne'] = $_POST['tahun'];
		}else{
			$month = date('m');
			$year = date('Y');
			$data['bulanne'] = date('m');
			$data['tahunne'] = date('Y');
		}

		$data['data_array'] = $this->db->query("SELECT a.transaction_id, a.transaction_number, a.transaction_date, a.transaction_total, a.transaction_discount, a.transaction_grand_total, b.member_name FROM transaction a INNER JOIN member b ON a.member_id = b.member_id WHERE a.transaction_type = 1 AND MONTH(a.transaction_date) = '".$month."' AND YEAR(a.transaction_date) = '".$year."'")->result_array();

		show("report/cash_list", "Riwayat Penjualan Tunai", $data);
	}

	public function credit()
	{
		if(isset($_POST['tahun']) && isset($_POST['bulan']))
		{
			$month = $_POST['bulan'];
			$year = $_POST['tahun'];
			$data['bulanne'] = $_POST['bulan'];
			$data['tahunne'] = $_POST['tahun'];
		}else{
			$month = date('m');
			$year = date('Y');
			$data['bulanne'] = date('m');
			$data['tahunne'] = date('Y');
		}

		$data['data_array'] = $this->db->query("SELECT a.transaction_id, a.transaction_number, a.transaction_date, a.transaction_total, a.transaction_discount, a.transaction_grand_total, a.transaction_down_payment, b.member_name, SUM(c.recieve_amount) AS jumlah_bayar FROM transaction a INNER JOIN member b ON a.member_id = b.member_id INNER JOIN transaction_credit_recieve c ON a.transaction_id = c.transaction_id WHERE a.transaction_type = 2 AND MONTH(a.transaction_date) = '".$month."' AND YEAR(a.transaction_date) = '".$year."' GROUP BY a.transaction_number")->result_array();

		show("report/credit_list", "Riwayat Penjualan Kredit", $data);
	}

	public function persediaan()
	{
		$data['data_array'] = $this->Item_model->getAll();
		$data['data_type'] = $this->Db_umum->getSelect("type_id, type_name","item_type")->result_array();

		show("report/item_list", "Barang", $data);
	}

	public function labarugi()
	{
		if(isset($_POST['tahun']) && isset($_POST['bulan']))
		{
			$month = $_POST['bulan'];
			$year = $_POST['tahun'];
			$data['bulanne'] = $_POST['bulan'];
			$data['tahunne'] = $_POST['tahun'];
		}else{
			$month = date('m');
			$year = date('Y');
			$data['bulanne'] = date('m');
			$data['tahunne'] = date('Y');
		}
		// $data['data_array'] = $this->Item_model->getAll();
		$data['pembelian'] = '';
		$data['penjualan'] = '';
		$getPembelian = $this->Db_umum->select("SELECT purchasing_grand_total FROM purchasing WHERE MONTH(purchasing_date) = '".$month."' AND YEAR(purchasing_date) = '".$year."' ");
		if($getPembelian)
		{
			foreach($getPembelian as $p)
			{
				$data['pembelian'] += $p->purchasing_grand_total;
			}
		}

		$getPenjualan = $this->Db_umum->select("SELECT transaction_grand_total FROM transaction WHERE MONTH(transaction_date) = '".$month."' AND YEAR(transaction_date) = '".$year."' ");
		if($getPenjualan)
		{
			foreach($getPenjualan as $p)
			{
				$data['penjualan'] += $p->transaction_grand_total;
			}
		}

		$data['data_array'] = $this->db->query("SELECT a.transaction_date AS tanggal, c.item_name AS nama_barang, c.item_number AS kode_barang, a.transaction_number, a.transaction_type, b.detail_qty, (c.item_purchase_price * b.detail_qty) AS harga_beli, (c.item_sold_price * b.detail_qty) AS harga_jual, a.transaction_discount AS diskon, ((c.item_sold_price * b.detail_qty) - ((c.item_sold_price * b.detail_qty) * (a.transaction_discount/100))) AS subtotal, ((c.item_sold_price * b.detail_qty) - ((c.item_sold_price * b.detail_qty) * (a.transaction_discount/100))) - (c.item_purchase_price * b.detail_qty) AS labarugi FROM transaction a INNER JOIN transaction_detail b ON a.transaction_id = b.transaction_id INNER JOIN item c ON b.item_id = c.item_id WHERE MONTH(a.transaction_date) = '".$month."' AND YEAR(a.transaction_date) = '".$year."' GROUP BY a.transaction_number,c.item_name")->result_array();

		show("report/labarugi", "Laba Rugi", $data);
	}

	public function printLabaRugi()
	{
		$month = $this->uri->segment(5);
		$year = $this->uri->segment(6);

		$data['data_array'] = $this->db->query("SELECT a.transaction_date AS tanggal, c.item_name AS nama_barang, c.item_number AS kode_barang, a.transaction_number, a.transaction_type, b.detail_qty, (c.item_purchase_price * b.detail_qty) AS harga_beli, (c.item_sold_price * b.detail_qty) AS harga_jual, a.transaction_discount AS diskon, ((c.item_sold_price * b.detail_qty) - ((c.item_sold_price * b.detail_qty) * (a.transaction_discount/100))) AS subtotal, ((c.item_sold_price * b.detail_qty) - ((c.item_sold_price * b.detail_qty) * (a.transaction_discount/100))) - (c.item_purchase_price * b.detail_qty) AS labarugi FROM transaction a INNER JOIN transaction_detail b ON a.transaction_id = b.transaction_id INNER JOIN item c ON b.item_id = c.item_id WHERE MONTH(a.transaction_date) = '".$month."' AND YEAR(a.transaction_date) = '".$year."' GROUP BY a.transaction_number,c.item_name")->result_array();
		$view = "report/printLabaRugi";
		$this->load->view($view, $data);
	}

	public function printTransaction()
	{
		$month = $this->uri->segment(5);
		$year = $this->uri->segment(6);

		$data['data_array'] = $this->db->query("SELECT f.type_name, d.detail_qty, e.item_name, a.purchasing_id, a.purchasing_number, a.purchasing_date, a.purchasing_total, a.purchasing_discount, a.purchasing_grand_total, b.member_name, c.supplier_name FROM purchasing a INNER JOIN member b ON a.member_id = b.member_id INNER JOIN supplier c ON a.supplier_id = c.supplier_id INNER JOIN purchasing_detail d ON a.purchasing_id = d.purchasing_id INNER JOIN item e ON d.item_id = e.item_id INNER JOIN item_type f ON e.type_id = f.type_id WHERE MONTH(a.purchasing_date) = '".$month."' AND YEAR(a.purchasing_date) = '".$year."' GROUP BY a.purchasing_number,e.item_name")->result_array();
		$data['sum_jumlah'] = $this->db->query("SELECT SUM(purchasing_grand_total) AS jumlah FROM purchasing WHERE MONTH(purchasing_date) = '".$month."' AND YEAR(purchasing_date) = '".$year."' ")->row();
		$data['sum_qty'] = $this->db->query("SELECT SUM(detail_qty) AS jumlah FROM purchasing_detail INNER JOIN purchasing ON purchasing_detail.purchasing_id = purchasing.purchasing_id WHERE MONTH(purchasing_date) = '".$month."' AND YEAR(purchasing_date) = '".$year."'")->row();
		$view = "report/printTransaction";
		$this->load->view($view, $data);
	}

	public function printCash()
	{
		$month = $this->uri->segment(5);
		$year = $this->uri->segment(6);

		$data['data_array'] = $this->db->query("SELECT f.type_name, d.detail_qty, e.item_name, a.transaction_id, a.transaction_number, a.transaction_date, a.transaction_total, a.transaction_discount, a.transaction_grand_total, b.member_name FROM transaction a INNER JOIN member b ON a.member_id = b.member_id INNER JOIN transaction_detail d ON a.transaction_id = d.transaction_id INNER JOIN item e ON d.item_id = e.item_id INNER JOIN item_type f ON e.type_id = f.type_id WHERE a.transaction_type = 1 AND MONTH(a.transaction_date) = '".$month."' AND YEAR(a.transaction_date) = '".$year."' GROUP BY a.transaction_number,e.item_name")->result_array();
		$data['sum_jumlah'] = $this->db->query("SELECT SUM(transaction_grand_total) AS jumlah FROM transaction WHERE transaction_type = 1 AND MONTH(transaction_date) = '".$month."' AND YEAR(transaction_date) = '".$year."'")->row();
		$data['sum_qty'] = $this->db->query("SELECT SUM(detail_qty) AS jumlah FROM transaction_detail INNER JOIN transaction ON transaction_detail.transaction_id = transaction.transaction_id WHERE transaction_type = 1 AND MONTH(transaction_date) = '".$month."' AND YEAR(transaction_date) = '".$year."'")->row();
		$view = "report/printCash";
		$this->load->view($view, $data);
	}

	public function printCredit()
	{
		$month = $this->uri->segment(5);
		$year = $this->uri->segment(6);

		$data['data_array'] = $this->db->query("SELECT f.type_name, d.detail_qty, e.item_name, a.transaction_id, a.transaction_number, a.transaction_date, a.transaction_total, a.transaction_discount, a.transaction_grand_total, a.transaction_down_payment, b.member_name, SUM(c.recieve_amount) AS jumlah_bayar, (a.transaction_grand_total - SUM(c.recieve_amount)) AS sisa_bayar FROM transaction a INNER JOIN member b ON a.member_id = b.member_id INNER JOIN transaction_credit_recieve c ON a.transaction_id = c.transaction_id INNER JOIN transaction_detail d ON a.transaction_id = d.transaction_id INNER JOIN item e ON d.item_id = e.item_id INNER JOIN item_type f ON e.type_id = f.type_id WHERE a.transaction_type = 2 AND MONTH(a.transaction_date) = '".$month."' AND YEAR(a.transaction_date) = '".$year."' GROUP BY a.transaction_number,e.item_name ")->result_array();
		$data['sum_dp'] = $this->db->query("SELECT SUM(recieve_amount) AS jumlah FROM transaction_credit_recieve WHERE recieve_type = 1 AND MONTH(recieve_create_date) = '".$month."' AND YEAR(recieve_create_date) = '".$year."'")->row();
		$data['sum_bayar'] = $this->db->query("SELECT SUM(recieve_amount) AS jumlah FROM transaction_credit_recieve WHERE MONTH(recieve_create_date) = '".$month."' AND YEAR(recieve_create_date) = '".$year."'")->row();
		$data['sum_jumlah'] = $this->db->query("SELECT SUM(transaction_grand_total) AS jumlah FROM transaction WHERE transaction_type = 2 AND MONTH(transaction_date) = '".$month."' AND YEAR(transaction_date) = '".$year."'")->row();
		$data['sum_qty'] = $this->db->query("SELECT SUM(detail_qty) AS jumlah FROM transaction_detail INNER JOIN transaction ON transaction_detail.transaction_id = transaction.transaction_id WHERE transaction_type = 2 AND MONTH(transaction_date) = '".$month."' AND YEAR(transaction_date) = '".$year."'")->row();
		
		$view = "report/printCredit";
		$this->load->view($view, $data);
	}
}