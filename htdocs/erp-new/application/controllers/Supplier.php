<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {
	public function index()
	{
		$data['data_array'] = $this->Db_umum->getSelect("supplier_id, supplier_number, supplier_name, supplier_address, supplier_phone, supplier_mail","supplier")->result_array();

		show("purchasing/supplier_list", "Supplier", $data);
	}
	function save()
	{
		$id = $this->input->post("id");
		if($id == 0){
			$data = array(
				"supplier_number" => $this->input->post("supplier_number"),
				"supplier_name" => $this->input->post("supplier_name"),
				"supplier_address" => $this->input->post("supplier_address"),
				"supplier_phone" => $this->input->post("supplier_phone"),
				"supplier_mail" => $this->input->post("supplier_mail"),
				"supplier_create_date" => date("Y-m-d H:i:s"),
				"supplier_state" => 1,
				);
			$this->Db_umum->insert("supplier",$data);
			$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Input data berhasil</div>');
		} else {
			$data = array(
				"supplier_number" => $this->input->post("supplier_number"),
				"supplier_name" => $this->input->post("supplier_name"),
				"supplier_address" => $this->input->post("supplier_address"),
				"supplier_phone" => $this->input->post("supplier_phone"),
				"supplier_mail" => $this->input->post("supplier_mail"),
				"supplier_last_update" => date("Y-m-d H:i:s")
				);
			$this->Db_umum->update("supplier","supplier_id",$id,$data);
			$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Edit data berhasil</div>');
		}
		redirect('panel/purchasing/listSupplier','refresh');
	}
	function delete($id)
	{
		$this->Db_umum->delete("supplier","supplier_id", $id);
		$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Hapus data berhasil</div>');
		redirect('panel/purchasing/listSupplier','refresh');
	}
}

/* End of file Supplier.php */
/* Location: ./application/controllers/Supplier.php */