<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Purchasing_model extends CI_Model {

	function getAll()
	{
		$this->db->distinct();
		$this->db->select("a.purchasing_id, a.purchasing_number, a.purchasing_date, a.purchasing_total, a.purchasing_discount, a.purchasing_grand_total, b.member_name, c.supplier_name");
		$this->db->from("purchasing a");
		$this->db->join("member b","a.member_id = b.member_id");
		$this->db->join("supplier c","a.supplier_id = c.supplier_id");
		$query = $this->db->get()->result_array();
		return $query;
	}
	function getDetail($id)
	{
		$this->db->distinct();
		$this->db->select("a.detail_qty, a.detail_price, (a.detail_qty * a.detail_price) as sub_total, b.item_name");
		$this->db->from("purchasing_detail a");
		$this->db->join("item b","a.item_id = b.item_id");
		$this->db->where("a.purchasing_id",$id);
		$query = $this->db->get()->result_array();
		return $query;
	}

}

/* End of file Purchasing_model.php */
/* Location: ./application/models/Purchasing_model.php */