<?php
class Db_umum extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	function getAllStatus($table,$status,$statusValue)
	{
		$this->db->select("*");
		$this->db->from($table);
		$this->db->where($status,$statusValue);
		return $this->db->get();
	}
	function getSelect($select,$table)
	{
		$this->db->select($select);
		$this->db->from($table);
		return $this->db->get();
	}
	function getById($select,$table,$id_table,$id)
	{
		$this->db->select($select);
		$this->db->where($id_table, $id);
		return $this->db->get($table);
	}
	function get_last_ten_entries()
	{
		$query = $this->db->get('entries', 10);
		return $query->result();
	}

	function get_db($db,$select,$where)
	{
		$query = $this->db->query("select $select from $db where $where");
		$num = $query->num_rows();
		if($num>0){
			return $query->result();
		}
		else{
			return 0;
		}
	}
	function row($sql)
	{
		$w = $this->db->query($sql);
		return $w->num_rows();
	}

	function select($sql)
	{
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		if($num>0){
			return $query->result();
		}
		else{
			return 0;
		}
	}

	function select_row($sql)
	{
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		if($num>0){
			return $query->row();
		}
		else{
			return 0;
		}
	}

	function select_row2($sql)
	{
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		if($num>0){
			return $query->row();
		}
		else{
			return 0;
		}
	}

	function insert($db,$data)
	{
		$this->db->insert($db, $data); 
		$insert_id = $this->db->insert_id();

		return  $insert_id;
	}
	function update($table,$id,$val,$data)
	{
		$this->db->where($id, $val);
		$this->db->update($table, $data); 
	}
	function delete($table, $id_table, $id)
	{
		$this->db->where($id_table, $id);
		$this->db->delete($table);
	}
	function select_where($db,$where)
	{
		$query = $this->db->get_where($db,$where);
		$num = $query->num_rows();
		if($num>0){
			return $query->result();
		}
		else{
			return 0;
		}
	}

	function cek_login($data)
	{
		$query = $this->db->get_where('user', $data);
		return $query;
	}

	function enumselect( $table, $field )
	{
		$type = $this->db->query( "SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'" )->row( 0 )->Type;
		preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
		$enum = explode("','", $matches[1]);
		return $enum;
	}

	public function getMaxKodeTagihan()
	{
		$q = $this->db->query("select MAX(RIGHT(kd_jurnal,2)) as kd_max from akun_jurnal");
		$kd = "";
		if($q->num_rows()>0)
		{
			foreach($q->result() as $k)
			{
				$tmp = ((int)$k->kd_max)+1;
				$kd = sprintf("%02s", $tmp);
			}
		}
		else
		{
			$kd = "01";
		}   
		return $kd;
	}

	public function getMaxNom()
	{
		$tahun = $this->uri->segment(3);
		$semester = $this->uri->segment(4);
		$kat = $this->uri->segment(5);
		$q = $this->db->query("select MID(kd_jadwal,3) as kd_max from account where id_tahunajar='".$tahun."' AND semester = '".$semester."' AND kat = '".$kat."'");
		$kd = "";
		if($q->num_rows()>0)
		{
			foreach($q->result() as $k)
			{
				$tmp = ((int)$k->kd_max)+1;
				$kd = sprintf("%02s", $tmp);
			}
		}
		else
		{
			$kd = "01";
		}   
		return $kd;
	}
	public function kodeJadwal()
	{
		$tahunnya = $this->uri->segment(3);
		$semesternya = $this->uri->segment(5);
		$katnya = $this->uri->segment(8);
		list($tahun) = $this->Db_umum->select("SELECT MID(tahun_ajar,3,2) AS awal, MID(tahun_ajar,8,2) AS akhir FROM tahun_ajar WHERE id = '".$tahunnya."' ");
		return $kode = $tahun->awal."".$tahun->akhir."-".substr($semesternya,0,3)."".substr($katnya,0,2);
	}

	public function kodeJadwal_2()
	{
		$tahunnya = $_POST['tahun'];
		$semesternya = $_POST['semester'];
		$katnya = $_POST['kategori'];
		list($tahun) = $this->Db_umum->select("SELECT MID(tahun_ajar,3,2) AS awal, MID(tahun_ajar,8,2) AS akhir FROM tahun_ajar WHERE id = '".$tahunnya."' ");
		return $kode = $tahun->awal."".$tahun->akhir."-".substr($semesternya,0,3)."".substr($katnya,0,2);
	}
}
