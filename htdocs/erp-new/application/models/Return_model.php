<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Return_model extends CI_Model {

	function getAllPurchasing()
	{
		$this->db->select("a.return_id, a.return_description, a.return_create_date, a.return_type, a.return_state");
		$this->db->from("return a");
		$this->db->where("a.return_type",1);
		$this->db->order_by("a.return_id","DESC");
		$query = $this->db->get();
		return $query->result_array();
	}
	function getAllTransaction()
	{
		$this->db->select("a.return_id, a.return_description, a.return_create_date, a.return_type, a.return_state");
		$this->db->from("return a");
		$this->db->where("a.return_type",2);
		$this->db->order_by("a.return_id","DESC");
		$query = $this->db->get();
		return $query->result_array();
	}
	function getDetail($id)
	{
		$this->db->select("a.detail_quantity as qty, b.item_number as number, b.item_name as name");
		$this->db->from("return_detail a");
		$this->db->join("item b","a.item_id = b.item_id");
		$this->db->where("a.return_id",$id);
		$query = $this->db->get();
		return $query->result_array();
	}

}

/* End of file Return_model.php */
/* Location: ./application/models/Return_model.php */