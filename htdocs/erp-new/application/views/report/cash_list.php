<style type="text/css">
	.table thead tr th{
		text-align: center;
	}
	.table thead tr th, .table tbody tr td, .table tfoot tr th{
		vertical-align: middle;
		cursor: pointer;
	}
</style>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header">
				<form action="" method="POST" class="col-md-6">
					<select name="bulan">
						<option value="01">Januari</option>
						<option value="02">Februari</option>
						<option value="03">Maret</option>
						<option value="04">April</option>
						<option value="05">Mei</option>
						<option value="06">Juni</option>
						<option value="07">Juli</option>
						<option value="08">Agustus</option>
						<option value="09">September</option>
						<option value="10">Oktober</option>
						<option value="11">Nopember</option>
						<option value="12">Desember</option>
					</select>

					<input name="tahun" placeholder="tahun" style="width:10%;" required>
					<button type="submit">Go!</button>
				</form>
				<a href="<?=base_url();?>panel/report/transactionCash/print/<?=$bulanne;?>/<?=$tahunne;?>" class="btn btn-primary" target="_blank">Cetak</a>
					<h3 class="box-title pull-right">Riwayat Penjualan Tunai</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12 table-responsive">
							<table class="table table-striped table-bordered table-hover" id="example">
								<thead>
									<tr>
										<th>#</th>
										<th>Tanggal</th>
										<th>Nomor Beli</th>
										<th>Anggota</th>
										<th>Jumlah Bayar</th>
									</tr>
								</thead>
								<tbody>
									<?php $jumlahe = 0; $start = 0; foreach ($data_array as $data) { ?>
									<tr>
										<td><?= ++$start ?>.</td>
										<td><?= indonesianDate($data["transaction_date"]) ?></td>
										<td><?= $data["transaction_number"] ?></td>
										<td><?= $data["member_name"] ?></td>
										<td>Rp <?= number_format($data['transaction_grand_total'],0, ",","."); ?></td>
									</tr>
									<?php $jumlahe += $data['transaction_grand_total']; } ?>
								</tbody>
								<tfoot>
									<th></th>
									<th></th>
									<th></th>
									<th>Total Penjualan</th>
									<th>Rp <?= number_format($jumlahe,0, ",","."); ?></th>
								</tfoot>
							</table>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
$(document).ready(function() {
	$('#example').DataTable( {
	} );
} );
	
</script>