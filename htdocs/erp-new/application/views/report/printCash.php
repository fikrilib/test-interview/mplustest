<style type="text/css">
@page {
  size: A4 ;
}
	.table thead tr th{
		text-align: center;

	}
	.table thead tr th, .table tbody tr td, .table tfoot tr th{
		vertical-align: middle;
		cursor: pointer;
		padding:3px 3px 3px 3px;
	}
	.table th td{

	}

tr.group,
tr.group:hover {
    background-color: #ddd !important;
}
.box{
    border-collapse: collapse;
    border-width: 1px;
    border-style: solid
}
</style>
<?php
	$array_tanggal = array();
	$array_transaction_number = array();
	$array_anggota = array();
	$array_jumlah_bayar = array();
	$array_nama_barang = array();
	$array_detail_qty = array();
	$array_diskon = array();
	$array_rowspan = array();
	$tempid = array();
	$i = 0;
	foreach ($data_array as $data) {
	    if($tempid != $data['transaction_number']){

	        $tempid = $data['transaction_number'];
	        $rowspan = 1;       
			$array_tanggal[] = $data['transaction_date'];
			$array_transaction_number[] = $data['transaction_number'];
			$array_anggota[] = $data['member_name'];
			$array_jumlah_bayar[] = $data['transaction_grand_total'];
			$array_nama_barang[] = $data['type_name'];
			$array_detail_qty[] = $data['detail_qty'];
			$array_diskon[] = $data['transaction_discount'];      
	        $array_rowspan[$i] = $rowspan;

	        //Set another temp value to set same rowspan in mached category id  
	        $same_id=1; 
	    }else{

	        $rowspan++;     
			$array_tanggal[] = $data['transaction_date'];
			$array_transaction_number[] = $data['transaction_number'];
			$array_anggota[] = $data['member_name'];
			$array_jumlah_bayar[] = $data['transaction_grand_total'];
			$array_nama_barang[] = $data['type_name'];
			$array_detail_qty[] = $data['detail_qty'];
			$array_diskon[] = $data['transaction_discount'];   

	        //Set another temp value to set same rowspan in mached category id          
	        $same_id++;     
	        for($j=0; $j<$same_id; $j++){
	            $array_rowspan[$i-($j)] = $rowspan;
	        }

	    }       

	    $i++;  
	}

?>
<center>
	<h2>Laporan Penjualan Tunai</h2>
	<h3 style="margin-bottom: 10px;">Periode <?=namabulan_2($this->uri->segment(5)).' '.$this->uri->segment(6);?></h3>
</center>
<table class="table box" id="example" border="1">
	<thead>
		<tr>
			<th>Tanggal</th>
			<th>No. Beli</th>
			<th>Anggota</th>
			<th>Nama Barang</th>
			<th>Qty</th>
			<th>Diskon</th>
			<th>Jumlah Bayar</th>
		</tr>
	</thead>
	<tbody>
		<?php $tempid2 = ""; $sumJumlahBayar = 0; $start = 0; 
			foreach ($array_transaction_number as $key => $val) { 
				if($tempid2 != $val){
					$tempid2 = $val;
		?>
		<tr>
			<td><?= $array_tanggal[$key] ?></td>
			<td rowspan="<?=$array_rowspan[$key];?>"><?= $array_transaction_number[$key] ?></td>
			<td rowspan="<?=$array_rowspan[$key];?>"><?= $array_anggota[$key] ?></td>
			<td rowspan="<?=$array_rowspan[$key];?>"><?= $array_nama_barang[$key] ?></td>
			<td><?= $array_detail_qty[$key] ?></td>
			<td><?= $array_diskon[$key] ?></td>
			<td rowspan="<?=$array_rowspan[$key];?>">Rp <?= number_format($array_jumlah_bayar[$key],0, ",","."); ?></td>
		</tr>
		<?php 
				}else{
					?>
					<tr>
						<td><?= $array_tanggal[$key] ?></td>
						<!-- <td><?= $array_nama_barang[$key] ?></td> -->
						<td><?= $array_detail_qty[$key] ?></td>
						<td><?= $array_diskon[$key] ?></td>
					</tr>
			<?php
				}
			$sumJumlahBayar += $array_jumlah_bayar[$key];
		} ?>
	</tbody>
	<tfoot>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th><?=$sum_qty->jumlah;?></th>
		<th></th>
		<th>Rp <?=number_format($sum_jumlah->jumlah,0, ",",".");?></th>
	</tfoot>
</table>	