<style type="text/css">
	.table thead tr th{
		text-align: center;
	}
	.table thead tr th, .table tbody tr td, .table tfoot tr th{
		vertical-align: middle;
		cursor: pointer;
	}
</style>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Penerimaan Piutang Kredit</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<?=$this->session->userdata('message_action') ?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 table-responsive">
							<table class="table table-striped table-bordered table-hover" id="generalTable">
								<thead>
									<tr>
										<th>#</th>
										<th>Tanggal</th>
										<th>Jatuh Tempo</th>
										<th>Nomor Beli</th>
										<th>Anggota</th>
										<th>Jumlah Bayar</th>
										<th>Sisa Pembayaran</th>
										<th style="width: 100px;">Opsi</th>
									</tr>
								</thead>
								<tbody>
									<?php $start = 0; foreach ($data_array as $data) { ?>
									<?php
									if(date("Y-m-d") > $data['transaction_due_date']){
										$transaction_grand_total = $data['transaction_grand_total'] + (0.1 * $data['transaction_grand_total']);
									} else {
										$transaction_grand_total = $data['transaction_grand_total'];
									}
									?>
									<tr>
										<td><?= ++$start ?>.</td>
										<td><?= indonesianDate($data["transaction_date"]) ?></td>
										<td><?= indonesianDate($data["transaction_due_date"]) ?></td>
										<td><?= $data["transaction_number"] ?></td>
										<td><?= $data["member_name"] ?></td>
										<td>Rp <?= number_format($transaction_grand_total,0, ",","."); ?></td>
										<td>Rp <?= number_format($transaction_grand_total - $data['has_receive'],0, ",","."); ?></td>
										<td>
											<?php if($transaction_grand_total - $data['has_receive'] > 0){ ?>
											<a href="javascript:recieve(<?= $data['transaction_id']; ?>,<?= $transaction_grand_total; ?>)" id="recieve<?= $data['transaction_id']; ?>" data-number="<?= $data["transaction_number"] ?>" data-member="<?= $data['member_id']; ?>" data-toggle="tooltip" data-placement="top" title="Penerimaan <?= $data["transaction_number"] ?>" class="btn btn-sm btn-primary" style="width: 50%"><i class="fa fa-dollar"></i></a>
											<?php } else { ?>
											<a href="javascript:recieve(<?= $data['transaction_id']; ?>,<?= $transaction_grand_total; ?>)" id="recieve<?= $data['transaction_id']; ?>" data-number="<?= $data["transaction_number"] ?>" data-toggle="tooltip" data-placement="top" title="Detail <?= $data["transaction_number"] ?>" class="btn btn-sm btn-success" style="width: 50%"><i class="fa fa-info"></i></a>

											<?php } ?>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="recieveModal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Penerimaan Piutang Kredit</h4>
			</div>
			<div class="modal-body">
				<div class="row">	
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-12">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>Penerimaan</th>
											<th>Tanggal</th>
											<th>Sejumlah</th>
										</tr>
									</thead>
									<tbody id="result-data">

									</tbody>
									<tfoot>
										<tr style="border-top: 2px solid #EEE">
											<th class="text-center" colspan="2">Sisa</th>
											<th><span id="remain-receive"></span></th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
					<div class="col-md-6" style="border-left: 2px solid #dadada">
						<div class="row">
							<div class="col-md-12">
								<form class="form-horizontal" method="POST" action="<?= base_url(); ?>panel/receivable/listReceivable/save" onsubmit="return checkRecive()">
									<div class="form-group">
										<label class="col-md-3 control-label">Penerimaan</label>
										<div class="col-md-9">
											<input type="text" class="form-control rupiah" name="recieve_amount" placeholder="Rp 100.000" required="">
											<input type="hidden" name="id">
											<input type="hidden" name="id_member" id="myIdMember">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-9 col-md-offset-3">
											<button type="submit" class="btn btn-primary" style="width: 100%">Simpan</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<a data-dismiss="modal" class="btn btn-warning btn-flat">Tutup</a>
			</div>
		</form>
	</div>
</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		
	});
	function recieve(id,total){
		$.ajax({
			url: "<?= site_url()?>panel/receivable/listReceivable/detail/"+id,
			beforeSend: function(){
				$("input[name='id']").val(id);
				$("button[type='submit']").prop("disabled", false);
				$("#result-data").html('<tr><td colspan="3" class="text-center"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></td></tr>');
				$('#myModalLabel').text("Penerimaan Piutang Kredit "+$("#recieve"+id).data("number"));
				$('#myIdMember').val($("#recieve"+id).data("member"));
				$('#recieveModal').modal('toggle');
			},
			success: function(response) {	
				$("#result-data").html("");
				var data = JSON.parse(response);
				$("input[name='id']").val(id);
				var remain = total - data.total.replace(/\D/g,'');
				$("#remain-receive").text(toRp(remain));
				if(remain <= 0){
					$("button[type='submit']").prop("disabled", true);
				} else {
					$("button[type='submit']").prop("disabled", false);
				}
				$.each(data.detail_recieve, function(key,value) {
					$("#result-data").append('<tr><td class="text-center">'+value.sort+'</td><td>'+value.date+'</td><td>'+value.pay+'</td></tr>');
				});	
			},
			complete: function(){
			}
		})
	}
	function checkRecive(){
		var remain = $("#remain-receive").text().replace(/\D/g,'');
		var recieve = $("input[name='recieve_amount']").val().replace(/\D/g,'');

		if(recieve > remain){
			alert("Penerimaan melebihi total piutang!");
			return false;
		}
	}
	
</script>