<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Data Anggota</h3>
					<div class="pull-right">
						<a type="button" href="javascript:addNew()" class="btn btn-primary">Tambah Data</a>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<?=$this->session->userdata('message_action') ?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 table-responsive">
							<table class="table table-striped table-bordered table-hover" id="generalTable">
								<thead>
									<tr>
										<th>#</th>
										<th>Username</th>
										<th>Jenis</th>
										<th style="width: 100px;">Opsi</th>
									</tr>
								</thead>
								<tbody>
									<?php $start = 0; foreach ($data_array as $data) { ?>
									<tr>
										<td><?= ++$start ?>.</td>
										<td><?= $data["username"] ?></td>
										<td><?= $data["jenis"] ?></td>
										<td>
											<a href="javascript:update(<?= $data['id']; ?>)" id="update<?= $data['id']; ?>" data-name="<?= $data["username"] ?>" data-jenis="<?= $data["jenis"] ?>" data-password="<?= $data["password"] ?>" data-toggle="tooltip" data-placement="top" title="Edit <?= $data["username"] ?>" class="btn btn-sm btn-warning btn-flat"><i class="fa fa-edit"></i></a>
											<a href="<?= base_url()?>panel/user/listUser/delete/<?= $data['id']; ?>" onclick="return confirm('Yakin akan menghapus data <?= $data["username"] ?>?')" data-toggle="tooltip" data-placement="top" title="Hapus <?= $data["username"] ?>" class="btn btn-sm btn-danger btn-flat"><i class="fa fa-trash-o"></i></a>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="formModal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Input Data Anggota</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" action="<?= base_url()?>panel/user/listUser/save"  method="post">
					<div class="form-group">
						<label class="col-md-3 control-label">Username</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="username" placeholder="Nama" required/>								
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Password</label>
						<div class="col-md-7">
							<input type="password" class="form-control" name="password2" placeholder="Password"/>								
						</div>
					</div>
					<input type="hidden" name="password">
					<div class="form-group">
						<label class="col-md-3 control-label">Jenis</label>
						<div class="col-md-7">
							<select name="jenis" class="form-control" required>
								<option value=""> - Silahkan Pilih - </option>
								<option value="user">Admin</option>
								<option value="pimpinan">Pimpinan</option>
								<option value="pembelian">Pembelian</option>
								<option value="penjualan">Penjualan</option>
							</select>						
						</div>
					</div>
					<input type="hidden" name="id" id="id" />
				</div>
				<div class="modal-footer">
					<a data-dismiss="modal" class="btn btn-warning btn-flat">Batal</a>
					<button type="submit" id="submit-form" class="btn btn-primary btn-flat">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#formModal').on('shown.bs.modal', function () {
			$("input[name='username']").focus();
		})  
	});
	function addNew(){
		$("input[name='username']").val("");
		$("input[name='password2']").val("");
		$("input[name='jenis']").val("");
		$("#myModalLabel").text("Input Data User");
		$("#id").val(0);
		$("#submit-form").text("Simpan");
		$("#formModal").modal("show");
	}
	function update(id){
		$("input[name='username']").val($("#update"+id).data("name"));
		$("input[name='password']").val($("#update"+id).data("password"));
		$("input[name='password2']").val($("#update"+id).data("password2"));
		$("input[name='jenis']").val($("#update"+id).data("jenis"));
		$("#myModalLabel").text("Edit Data User");
		$("#id").val(id);
		$("#submit-form").text("Edit");
		$("#formModal").modal("show");
	}
</script>