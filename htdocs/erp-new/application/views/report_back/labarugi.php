<style type="text/css">
	.table thead tr th{
		text-align: center;
	}
	.table thead tr th, .table tbody tr td, .table tfoot tr th{
		vertical-align: middle;
		cursor: pointer;
	}

tr.group,
tr.group:hover {
    background-color: #ddd !important;
}
</style>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header">
				<form action="" method="POST" class="col-md-6">
					<select name="bulan">
						<option value="01">Januari</option>
						<option value="02">Februari</option>
						<option value="03">Maret</option>
						<option value="04">April</option>
						<option value="05">Mei</option>
						<option value="06">Juni</option>
						<option value="07">Juli</option>
						<option value="08">Agustus</option>
						<option value="09">September</option>
						<option value="10">Oktober</option>
						<option value="11">Nopember</option>
						<option value="12">Desember</option>
					</select>

					<input name="tahun" placeholder="tahun" style="width:10%;" required>
					<button type="submit">Go!</button>
				</form>
					<a href="<?=base_url();?>panel/report/profitLoss/print/<?=$bulanne;?>/<?=$tahunne;?>" class="btn btn-primary" target="_blank">Cetak</a>
					<h3 class="box-title pull-right">Laba Rugi Bulan <?=namabulan_2($bulanne);?> Tahun <?=$tahunne;?></h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12 table-responsive">
							<table class="table table-striped table-bordered table-hover" id="example">
								<thead>
									<tr>
										<th>Tanggal</th>
										<th>No. Transaksi</th>
<!-- 										<th>Kode Barang</th> -->
										<th>Nama Barang</th>
										<th>Qty</th>
										<th>Harga Beli</th>
										<th>Harga Jual</th>
										<th>Diskon</th>
										<th>Subtotal</th>
										<th>Laba/Rugi</th>
									</tr>
								</thead>
								<tbody>
									<?php $sumPurchasePrice = 0; $sumSoldPrice = 0; $sumSubtotal = 0; $sumProfitLoss = 0; $start = 0; foreach ($data_array as $data) { ?>
									<tr>
										<td><?= $data["tanggal"] ?></td>
										<td><?= $data["transaction_number"] ?></td>
										<!-- <td><?= $data["kode_barang"] ?></td> -->
										<td><?= $data["nama_barang"] ?></td>
										<td><?= $data["detail_qty"] ?></td>
										<td>Rp <?= number_format($data['harga_beli'],0, ",","."); ?></td>
										<td>Rp <?= number_format($data['harga_jual'],0, ",","."); ?></td>
										<td><?= $data["diskon"] ?> %</td>
										<td>Rp <?= number_format($data['subtotal'],0, ",","."); ?></td>
										<td>Rp <?= number_format($data['labarugi'],0, ",","."); ?></td>
									</tr>
									<?php 
										$sumSoldPrice += $data['harga_jual'];
										$sumPurchasePrice += $data['harga_beli'];
										$sumSubtotal += $data['subtotal'];
										$sumProfitLoss += $data['labarugi'];
									} ?>
								</tbody>
								<tfoot>
									<th></th>
									<th></th>
									<th></th>
									<!-- <th></th> -->
									<th></th>
									<th>Rp <?=number_format($sumPurchasePrice,0, ",",".");?></th>
									<th>Rp <?=number_format($sumSoldPrice,0, ",",".");?></th>
									<th></th>
									<th>Rp <?=number_format($sumSubtotal,0, ",",".");?></th>
									<th>Rp <?=number_format($sumProfitLoss,0, ",",".");?></th>
								</tfoot>
							</table>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
$(document).ready(function() {
	$('#example').DataTable( {
		"bSort" : false,
        "columnDefs": [
            { "visible": false, "targets": 1 }
        ],
        "order": [[ 1, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(1, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="10">Nomor Transaksi: '+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
	} );

    $('#example tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === 1 && currentOrder[1] === 'asc' ) {
            table.order( [ 1, 'desc' ] ).draw();
        }
        else {
            table.order( [ 1, 'asc' ] ).draw();
        }
    } );
} );
	
</script>