<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Data Barang</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<?=$this->session->userdata('message_action') ?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 table-responsive">
							<table class="table table-striped table-bordered table-hover" id="example">
								<thead>
									<tr>
										<th>#</th>
										<th>Nomor Barang</th>
										<th>Nama</th>
										<th>Stok</th>
										<th>Jenis</th>
										<th>Harga Beli</th>
										<th>Harga Jual</th>
									</tr>
								</thead>
								<tbody>
									<?php $start = 0; foreach ($data_array as $data) { ?>
									<tr>
										<td><?= ++$start ?>.</td>
										<td><?= $data["item_number"] ?></td>
										<td><?= $data["item_name"] ?></td>
										<td><?= $data["item_stock"] ?></td>
										<td><?= $data["type_name"] ?></td>
										<td>Rp <?= number_format($data['item_purchase_price'],0, ",","."); ?></td>
										<td>Rp <?= number_format($data['item_sold_price'],0, ",","."); ?></td>
									</tr>
									<?php } ?>
								</tbody>
							</table>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
$(document).ready(function() {
	$('#example').DataTable( {
		dom: 'Bfrtip',
		buttons: [
			{
	            extend: 'print',
	            text: 'Print',
	            footer: true,
	            autoPrint: true,
	            pageSize: 'A4',
	            title: 'Laporan Persediaan Barang',
	            exportOptions: {
	                modifier: {
	                    page: 'current'
	                }
	            }
			}
		]
	} );
} );
	
</script>