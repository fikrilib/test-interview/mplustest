<section class="sidebar">
	<div class="user-panel">
		<div class="pull-left image">
			<img src="<?=base_url();?>assets/images/user.png" class="img-circle" alt="User Image">
		</div>
		<div class="pull-left info">
			<p><?=ucfirst($this->session->userdata('user'))?></p>
			<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
		</div>
	</div>
<!--  <form action="#" method="get" class="sidebar-form">
    <div class="input-group">
      <input type="text" name="q" class="form-control" placeholder="Search...">
      <span class="input-group-btn">
        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
        </button>
      </span>
    </div>
  </form> -->
  <ul class="sidebar-menu">
  	<li class="header">MAIN MENU</li>
  	<li class="treeview">
  		<a href="#">
  			<i class="fa fa-dashboard"></i> <span>Dashboard</span>
  			<span class="pull-right-container">
  				<i class="fa fa-angle-left pull-right"></i>
  			</span>
  		</a>
  		<ul class="treeview-menu">
  			<li><a href="<?=base_url();?>assets/index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
  			<li><a href="<?=base_url();?>assets/index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
  		</ul>
  	</li>
  </ul>
</section>