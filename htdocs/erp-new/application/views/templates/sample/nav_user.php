<?php $jenis=$this->session->userdata('jenis'); ?>
<section class="sidebar">
	<div class="user-panel">
		<div class="pull-left image">
			<img src="<?=base_url();?>assets/images/user.png" class="img-circle" alt="User Image">
		</div>
		<div class="pull-left info">
			<p><?=ucfirst($this->session->userdata('user'))?></p>
			<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
		</div>
	</div>
	<ul class="sidebar-menu">
		<li class="header">MAIN MENU</li>
		<li class="treeview" data-menu="dashboard">
			<a href="<?= base_url(); ?>panel/dashboard">
				<i class="fa fa-dashboard"></i> <span>Dashboard</span>
			</a>
		</li>
	<?php
		if($jenis == "user"){
	?>
		<li class="treeview" data-menu="user">
			<a href="<?= base_url(); ?>panel/user/listUser">
				<i class="fa fa-users"></i> <span>User</span>
			</a>
		</li>
		<li class="treeview" data-menu="member">
			<a href="<?= base_url(); ?>panel/member/listMember">
				<i class="fa fa-users"></i> <span>Anggota</span>
			</a>
		</li>
		<li class="treeview" data-menu="items">
			<a href="#">
				<i class="fa fa-cubes"></i> <span>Barang</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li data-parent="listItem"><a href="<?=base_url();?>panel/items/listItem"><i class="fa fa-circle-o"></i>Data Barang</a></li>
				<li data-parent="listType"><a href="<?=base_url();?>panel/items/listType"><i class="fa fa-circle-o"></i>Master Jenis</a></li>
				<li data-parent="listType"><a href="<?=base_url();?>panel/items/stockCard"><i class="fa fa-circle-o"></i>Kartu Stock</a></li>
				<li data-parent="listType"><a href="<?=base_url();?>panel/receivable/receivableCard"><i class="fa fa-circle-o"></i>Kartu Piutang</a></li>
			</ul>
		</li>
		<li class="treeview" data-menu="purchasing">
			<a href="#">
				<i class="fa fa-shopping-bag"></i> <span>Pembelian</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li data-parent="formPurchasing"><a href="<?=base_url();?>panel/purchasing/formPurchasing"><i class="fa fa-circle-o"></i>Input Pembelian</a></li>
				<li data-parent="listPurchasing"><a href="<?=base_url();?>panel/purchasing/listPurchasing"><i class="fa fa-circle-o"></i>Riwayat Pembelian</a></li>
				<li data-parent="listSupplier"><a href="<?=base_url();?>panel/purchasing/listSupplier"><i class="fa fa-circle-o"></i>Master Supplier</a></li>
			</ul>
		</li>
		<li class="treeview" data-menu="transaction">
			<a href="#">
				<i class="fa fa-money"></i> <span>Penjualan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?= base_url('panel/transaction/cash/barcode')?>"><i class="fa fa-circle-o text-red"></i> <span>Transaksi Barcode</span></a></li>
				<li data-parent="cash">
					<a href="#">
						<i class="fa fa-circle"></i> <span>Tunai</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
						<ul class="treeview-menu">
							<li data-parent="formCash"> <a href="<?= base_url('panel/transaction/cash/formCash')?>"><i class="fa fa-circle-o"></i><span>Input Transaksi Tunai</span></a></li>
							<li data-parent="listCash"> <a href="<?= base_url('panel/transaction/cash/listCash')?>"><i class="fa fa-circle-o"></i><span>Riwayat Transaksi Tunai</span></a></li>    
						</ul>
					</a>
				</li> 
				<li data-parent="credit">
					<a href="#">
						<i class="fa fa-circle"></i> <span>Kredit</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
						<ul class="treeview-menu">
							<li data-parent="formCredit"> <a href="<?= base_url('panel/transaction/credit/formCredit')?>"><i class="fa fa-circle-o"></i><span>Input Transaksi Kredit</span></a></li>
							<li data-parent="listCredit"> <a href="<?= base_url('panel/transaction/credit/listCredit')?>"><i class="fa fa-circle-o"></i><span>Riwayat Transaksi Kredit</span></a></li>    
						</ul>
					</a>
				</li>       
			</ul>
		</li>
		<li class="treeview" data-menu="receivable">
			<a href="<?= base_url(); ?>panel/receivable/listReceivable">
				<i class="fa fa-dollar"></i> <span>Penerimaan Piutang</span>
			</a>
		</li>
		<li class="treeview" data-menu="returnItem">
			<a href="#">
				<i class="fa fa-exchange"></i> <span>Retur</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li data-parent="listReturnPurchasing"><a href="<?=base_url();?>panel/returnItem/listReturnPurchasing"><i class="fa fa-circle-o"></i>Pembelian</a></li>
				<li data-parent="listReturnTransaction"><a href="<?=base_url();?>panel/returnItem/listReturnTransaction"><i class="fa fa-circle-o"></i>Penjualan</a></li>
			</ul>
		</li>
		<li class="treeview" data-menu="report">
			<a href="#">
				<i class="fa fa-file-excel-o"></i> <span>Laporan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li data-parent="purchasing"><a href="<?=base_url();?>panel/report/purchasing"><i class="fa fa-circle-o"></i>Laporan Pembelian</a></li>
				<li data-parent="transactionCash"><a href="<?=base_url();?>panel/report/transactionCash"><i class="fa fa-circle-o"></i>Laporan Penjualan Tunai</a></li>
				<li data-parent="transactionCredit"><a href="<?=base_url();?>panel/report/transactionCredit"><i class="fa fa-circle-o"></i>Laporan Penjualan Kredit</a></li>
				<li data-parent="stock"><a href="<?=base_url();?>panel/report/stock"><i class="fa fa-circle-o"></i>Laporan Persediaan</a></li>
				<li data-parent="profitLoss"><a href="<?=base_url();?>panel/report/profitLoss"><i class="fa fa-circle-o"></i>Laporan Laba Rugi</a></li>
			</ul>
		</li>
	<?php 
	} 
		if($jenis == "penjualan"){
	?>
		<li class="treeview" data-menu="transaction">
			<a href="#">
				<i class="fa fa-money"></i> <span>Penjualan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?= base_url('panel/transaction/cash/barcode')?>"><i class="fa fa-circle-o text-red"></i> <span>Transaksi Barcode</span></a></li>
				<li data-parent="cash">
					<a href="#">
						<i class="fa fa-circle"></i> <span>Tunai</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
						<ul class="treeview-menu">
							<li data-parent="formCash"> <a href="<?= base_url('panel/transaction/cash/formCash')?>"><i class="fa fa-circle-o"></i><span>Input Transaksi Tunai</span></a></li>
							<li data-parent="listCash"> <a href="<?= base_url('panel/transaction/cash/listCash')?>"><i class="fa fa-circle-o"></i><span>Riwayat Transaksi Tunai</span></a></li>    
						</ul>
					</a>
				</li> 
				<li data-parent="credit">
					<a href="#">
						<i class="fa fa-circle"></i> <span>Kredit</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
						<ul class="treeview-menu">
							<li data-parent="formCredit"> <a href="<?= base_url('panel/transaction/credit/formCredit')?>"><i class="fa fa-circle-o"></i><span>Input Transaksi Kredit</span></a></li>
							<li data-parent="listCredit"> <a href="<?= base_url('panel/transaction/credit/listCredit')?>"><i class="fa fa-circle-o"></i><span>Riwayat Transaksi Kredit</span></a></li>    
						</ul>
					</a>
				</li>       
			</ul>
		</li>
		<li class="treeview" data-menu="report">
			<a href="#">
				<i class="fa fa-file-excel-o"></i> <span>Laporan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li data-parent="transactionCash"><a href="<?=base_url();?>panel/report/transactionCash"><i class="fa fa-circle-o"></i>Laporan Penjualan Tunai</a></li>
				<li data-parent="transactionCredit"><a href="<?=base_url();?>panel/report/transactionCredit"><i class="fa fa-circle-o"></i>Laporan Penjualan Kredit</a></li>
				<li data-parent="stock"><a href="<?=base_url();?>panel/report/stock"><i class="fa fa-circle-o"></i>Laporan Persediaan</a></li>
			</ul>
		</li>
	<?php 
	} 
		if($jenis == "pembelian"){
	?>
		<li class="treeview" data-menu="purchasing">
			<a href="#">
				<i class="fa fa-shopping-bag"></i> <span>Pembelian</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li data-parent="formPurchasing"><a href="<?=base_url();?>panel/purchasing/formPurchasing"><i class="fa fa-circle-o"></i>Input Pembelian</a></li>
				<li data-parent="listPurchasing"><a href="<?=base_url();?>panel/purchasing/listPurchasing"><i class="fa fa-circle-o"></i>Riwayat Pembelian</a></li>
				<li data-parent="listSupplier"><a href="<?=base_url();?>panel/purchasing/listSupplier"><i class="fa fa-circle-o"></i>Master Supplier</a></li>
			</ul>
		</li>
		<li class="treeview" data-menu="report">
			<a href="#">
				<i class="fa fa-file-excel-o"></i> <span>Laporan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li data-parent="purchasing"><a href="<?=base_url();?>panel/report/purchasing"><i class="fa fa-circle-o"></i>Laporan Pembelian</a></li>
				<li data-parent="stock"><a href="<?=base_url();?>panel/report/stock"><i class="fa fa-circle-o"></i>Laporan Persediaan</a></li>
			</ul>
		</li>
	<?php 
	} 
		if($jenis == "pimpinan"){
	?>
		<li class="treeview" data-menu="items">
			<a href="#">
				<i class="fa fa-cubes"></i> <span>Barang</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li data-parent="listItem"><a href="<?=base_url();?>panel/items/listItem"><i class="fa fa-circle-o"></i>Data Barang</a></li>
				<li data-parent="listType"><a href="<?=base_url();?>panel/items/listType"><i class="fa fa-circle-o"></i>Master Jenis</a></li>
			</ul>
		</li>
		<li class="treeview" data-menu="report">
			<a href="#">
				<i class="fa fa-file-excel-o"></i> <span>Laporan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li data-parent="purchasing"><a href="<?=base_url();?>panel/report/purchasing"><i class="fa fa-circle-o"></i>Laporan Pembelian</a></li>
				<li data-parent="transactionCash"><a href="<?=base_url();?>panel/report/transactionCash"><i class="fa fa-circle-o"></i>Laporan Penjualan Tunai</a></li>
				<li data-parent="transactionCredit"><a href="<?=base_url();?>panel/report/transactionCredit"><i class="fa fa-circle-o"></i>Laporan Penjualan Kredit</a></li>
				<li data-parent="stock"><a href="<?=base_url();?>panel/report/stock"><i class="fa fa-circle-o"></i>Laporan Persediaan</a></li>
				<li data-parent="profitLoss"><a href="<?=base_url();?>panel/report/profitLoss"><i class="fa fa-circle-o"></i>Laporan Laba Rugi</a></li>
			</ul>
		</li>
		<?php } ?>
		</ul>
</section>