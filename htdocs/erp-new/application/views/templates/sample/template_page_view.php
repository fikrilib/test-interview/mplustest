<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
global $template;
$user=$this->session->userdata('user');
$pass=$this->session->userdata('pas');
$status=$this->session->userdata('status');
$jenis=$this->session->userdata('jenis');
if(!empty($user) AND !empty($pass) AND !empty($status) AND !empty($jenis))
{
	?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?= $page_title; ?>Koperasi Panel</title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="<?=base_url();?>assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?= base_url()?>assets/plugins/fontawesome/css/font-awesome.css" type="text/css">
		<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/select2/select2.css" type="text/css" />
		<link href="<?= base_url() ?>assets/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="<?= base_url()?>assets/plugins/datatable/media/css/dataTables.bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/plugins/datatable/extensions/Buttons/css/buttons.dataTables.css">
		<link rel="stylesheet" href="<?=base_url();?>assets/dist/css/AdminLTE.min.css">
		<link rel="stylesheet" href="<?=base_url();?>assets/dist/css/skins/_all-skins.min.css">
		<link rel="stylesheet" href="<?= base_url()?>assets/custom/css/backend.css">
		<script src="<?=base_url();?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>

	</head>
	<body class="hold-transition skin-blue-light sidebar-mini">

		<div class="wrapper">
			<header class="main-header">
				<a href="#" class="logo">
					<span class="logo-mini"><b>K</b></span>
					<span class="logo-lg"><b>Koperasi</b>Panel</span>
				</a>
				<nav class="navbar navbar-static-top">
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<li class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<?=base_url();?>assets/images/user.png" class="user-image" alt="User Image">
									<span class="hidden-xs"><?=$this->session->userdata('user')?></span>
								</a>
								<ul class="dropdown-menu">
									<!-- User image -->
									<li class="user-header">
										<img src="<?=base_url();?>assets/images/user.png" class="img-circle" alt="User Image">
										<p>
											<?=$this->session->userdata('jenis')?> - Web Developer
											<!-- <small>Member since Nov. 2012</small> -->
										</p>
									</li>
									<li class="user-footer">
										<!-- <div class="pull-left">
											<a href="#" class="btn btn-default btn-flat">Profile</a>
										</div> -->
										<div class="pull-right">
											<a href="<?=base_url();?>panel/logout" class="btn btn-default btn-flat">Keluar</a>
										</div>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
			</header>

			<!-- =============================================== -->
			<aside class="main-sidebar">
				<?php
					$this->load->view('templates/sample/nav_user'); 
				?>
			</aside>
			<!-- =============================================== -->
			
			<div class="content-wrapper">
				<?=alert_stock();?>
				<?php $this->load->view($view); ?>
				
			</div>
			<footer class="main-footer">
				<div class="pull-right hidden-xs">
					<b>Version</b>1.0.0
				</div>
				<strong>Copyright &copy; <?= date("Y"); ?> <a href="#">Koperasi Panel</a>.</strong> All rights
				reserved.
			</footer>
		</div>
		<!-- <script src="<?= base_url();?>assets/plugins/jQueryUI/jquery-ui.min.js"></script> -->
		<script src="<?= base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?= base_url();?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<script src="<?= base_url();?>assets/plugins/fastclick/fastclick.js"></script>
		<script src="<?= base_url();?>assets/dist/js/app.min.js"></script>
		<script src="<?= base_url()?>assets/plugins/datatable/media/js/jquery.dataTables.js"></script>
		<script src="<?= base_url()?>assets/plugins/datatable/media/js/dataTables.bootstrap.min.js"></script>
		<script src="<?= base_url();?>assets/plugins/select2/select2.js"></script>
		<script src="<?= base_url();?>assets/plugins/jquery.maskedinput.min.js"></script>
		<script src="<?= base_url();?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
		<script src="<?= base_url()?>assets/custom/js/backend.js"></script>
		<script src="<?= base_url()?>assets/custom/js/accounting.js"></script>
		<script type="text/javascript" language="javascript" src="<?= base_url(); ?>assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.js"></script>
		<script type="text/javascript" language="javascript" src="<?= base_url(); ?>assets/plugins/datatable/extensions/jszip.min.js"></script>
		<script type="text/javascript" language="javascript" src="<?= base_url(); ?>assets/plugins/datatable/extensions/pdfmake.min.js"></script>
		<script type="text/javascript" language="javascript" src="<?= base_url(); ?>assets/plugins/datatable/extensions/vfs_fonts.js"></script>
		<script type="text/javascript" language="javascript" src="<?= base_url(); ?>assets/plugins/datatable/extensions/Buttons/js/buttons.html5.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
	</body>
	</html>
	<?php 
}else
{
	redirect("panel");
}
?>