<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title pull-right">Kartu Persediaan</h3>
					<form method="POST" action="">
						<div class="col-md-4">
							<div class="input-group">
								<input class="form-control date3" type="text" name="start_date">
								<span class="input-group-addon">s/d</span>
								<input class="form-control date3" type="text" name="end_date">
							</div>
						</div>
						<div class="col-md-4">
							<select class="form-control select2" name="goods" required>
								<option value="">Pilih Barang</option>
								<?php foreach($array_goods as $data) { ?>
								<option value="<?= $data['item_id'] ?>"><?= $data['item_name'] ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-4" style="margin-top:-20px;">
							<button type="submit" class="btn btn-primary">Go</button>
						</div>
					</form>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<p align="center">Stok <strong><?= $goodsId; ?></strong> tanggal <?= $dateStart; ?> s/d <?= $dateEnd; ?></p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 table-responsive">
							<table class="table table-striped table-bordered table-hover" id="example">
								<thead>
									<tr>
										<th>Tanggal</th>
										<th>No Trans</th>
										<th>Keterangan</th>
										<th>Unit</th>
										<th>Harga</th>
										<th>Jumlah</th>
										<th>Unit</th>
										<th>Harga</th>
										<th>Jumlah</th>
										<th>Unit</th>
										<th>Harga</th>
										<th>Jumlah</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1; $jml_in = 0; $jml_out = 0; foreach($mutation_list as $mut) { 
										if($mut['status_mutasi'] == 1)
										{
											$keterangan = "Pembelian";
											$unitMasuk = $mut['stok_in'];
											$hargaMasuk = $mut['harga'];
											$jumlahMasuk = $unitMasuk * $hargaMasuk;

											$unitKeluar = "0";
											$hargaKeluar = "0";
											$jumlahKeluar = "0";

											$unitSaldo = $mut['stok_akhir'];
											$hargaSaldo = "0";
											$jumlahSaldo = "0";
										}elseif($mut['status_mutasi'] == 2)
										{
											$keterangan = "Penjualan";
											$unitMasuk = "0";
											$hargaMasuk = "0";
											$jumlahMasuk = "0";

											$unitKeluar = $mut['stok_out'];
											$hargaKeluar = $mut['harga'];
											$jumlahKeluar = $unitKeluar * $hargaKeluar;

											$unitSaldo = $mut['stok_akhir'];
											$hargaSaldo = "0";
											$jumlahSaldo = "0";
										}
										else{
											$keterangan = "";
											$unitMasuk = "";
											$hargaMasuk = "";
											$jumlahMasuk = "";

											$unitKeluar = "";
											$hargaKeluar = "";
											$jumlahKeluar = "";

											$unitSaldo = "";
											$hargaSaldo = "";
											$jumlahSaldo = "";
										}
										?>
										<tr <?php if($mut['status_mutasi'] == 4){echo"style='background-color:red;color:white;'";} ?>>
											<td><?= $mut['tanggal'] ?></td>
											<td><?= $mut['kode_transaksi'] ?></td>
											<td><?= $keterangan ?></td>
											<td><?= $unitMasuk ?></td>
											<td><?= $hargaMasuk ?></td>
											<td><?= $jumlahMasuk ?></td>
											<td><?= $unitKeluar ?></td>
											<td><?= $hargaKeluar ?></td>
											<td><?= $jumlahKeluar ?></td>
											<td><?= $unitSaldo ?></td>
											<td><?= $hargaSaldo ?></td>
											<td><?= $jumlahSaldo ?></td>
										</tr>
										
										<?php $no++; $jml_in += $mut['stok_in']; $jml_out += $mut['stok_out']; } ?>
									</tbody>
									<tfoot>
										<th></th>
										<th></th>
										<th></th>
										<th><?= $jml_in ?></th>
										<th><?= $jml_out ?></th>
										<th></th>
										<th></th>
										<th></th>
									</tfoot>
								</table>							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script>
		$(document).ready(function() {
			$.fn.datepicker.defaults.format = "yyyy-mm-dd";
			$(".date3").datepicker({ dateFormat: 'dd-mm-yy', autoclose: true, todayBtn: "linked", language: "id"});
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#example').DataTable( {
				dom: 'Bfrtip',
				buttons: [
				{
					extend: 'print',
					text: 'Print',
					footer: true,
					autoPrint: true,
					pageSize: 'A4',
					title: '<span style="font-size:17;text-align:center;">Kartu Stok <strong><?= $goodsId; ?></strong> tanggal <?= $dateStart; ?> s/d <?= $dateEnd; ?></span>',
					exportOptions: {
						modifier: {
							page: 'current'
						}
					}
				}
				]
			} );
		} );
		
	</script>