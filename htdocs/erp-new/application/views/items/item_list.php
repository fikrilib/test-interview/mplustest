<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Data Barang</h3>
					<div class="pull-right">
						<a type="button" href="javascript:addNew()" class="btn btn-primary">Tambah Data</a>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<?=$this->session->userdata('message_action') ?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 table-responsive">
							<table class="table table-striped table-bordered table-hover" id="generalTable">
								<thead>
									<tr>
										<th>#</th>
										<th>Nomor Barang</th>
										<th>Nama</th>
										<th>Stok</th>
										<th>Jenis</th>
										<th>Harga Beli</th>
										<th>Harga Jual</th>
										<th style="width: 100px;">Opsi</th>
									</tr>
								</thead>
								<tbody>
									<?php $start = 0; foreach ($data_array as $data) { 
										if($data['item_stock'] < 10)
										{
											$style = "style='background-color:#FF0000;'";
										}else{
											$style = "";
										}
										?>
									<tr <?=$style;?>>
										<td><?= ++$start ?>.</td>
										<td><?= $data["item_number"] ?></td>
										<td><?= $data["item_name"] ?></td>
										<td><?= $data["item_stock"] ?></td>
										<td><?= $data["type_name"] ?></td>
										<td>Rp <?= number_format($data['item_purchase_price'],0, ",","."); ?></td>
										<td>Rp <?= number_format($data['item_sold_price'],0, ",","."); ?></td>
										<td>
											<a href="javascript:update(<?= $data['item_id']; ?>)" id="update<?= $data['item_id']; ?>" data-number="<?= $data["item_number"] ?>" data-name="<?= $data["item_name"] ?>" data-stock="<?= $data["item_stock"] ?>" data-type="<?= $data["type_id"] ?>" data-purchase="<?= $data["item_purchase_price"] ?>" data-sold="<?= $data["item_sold_price"] ?>" data-toggle="tooltip" data-placement="top" title="Edit <?= $data["item_name"] ?>" class="btn btn-sm btn-warning btn-flat"><i class="fa fa-edit"></i></a>
											<a href="<?= base_url()?>panel/items/listItem/delete/<?= $data['item_id']; ?>" onclick="return confirm('Yakin akan menghapus data <?= $data["item_name"] ?>?')" data-toggle="tooltip" data-placement="top" title="Hapus <?= $data["item_name"] ?>" class="btn btn-sm btn-danger btn-flat"><i class="fa fa-trash-o"></i></a>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="formModal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Input Data Barang</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" action="<?= base_url()?>panel/items/listItem/save"  method="post">
					<div class="form-group">
						<label class="col-md-3 control-label">Nomor Barang</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="item_number" placeholder="Nomor Barang" required/>								
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Nama</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="item_name" placeholder="Nama Barang" required/>								
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Jenis</label>
						<div class="col-md-7">
							<select class="form-control" name="type_id">
								<option value="0" disabled="" selected="">Pilih Jenis</option>
								<?php foreach ($data_type as $data) { ?>
									<option value="<?= $data['type_id']; ?>"><?= $data['type_name']; ?></option>
								<?php } ?>
							</select>								
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Stok</label>
						<div class="col-md-7">
							<input type="number" class="form-control" name="item_stock" placeholder="Stock" onClick="this.select();" required/>								
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Harga Beli</label>
						<div class="col-md-7">
							<input type="text" class="form-control rupiah" name="item_purchase_price" placeholder="Harga Beli" required/>								
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Harga Jual</label>
						<div class="col-md-7">
							<input type="text" class="form-control rupiah" name="item_sold_price" placeholder="Harga Jual" required/>								
						</div>
					</div>
					<input type="hidden" name="id" id="id" />
				</div>
				<div class="modal-footer">
					<a data-dismiss="modal" class="btn btn-warning btn-flat">Batal</a>
					<button type="submit" id="submit-form" class="btn btn-primary btn-flat">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#formModal').on('shown.bs.modal', function () {
			$("input[name='item_number']").focus();
		})  
	});
	function addNew(){
		$("input[name='item_number']").val("");
		$("input[name='item_name']").val("");
		$("select[name='type_id']").val(0);
		$("input[name='item_stock']").val(0);
		$("input[name='item_purchase_price']").val(0);
		$("input[name='item_sold_price']").val(0);
		$("#myModalLabel").text("Input Data Barang");
		$("#id").val(0);
		$("#submit-form").text("Simpan");
		$("#formModal").modal("show");
	}
	function update(id){
		$("input[name='item_number']").val($("#update"+id).data("number"));
		$("input[name='item_name']").val($("#update"+id).data("name"));
		$("select[name='type_id']").val($("#update"+id).data("type"));
		$("input[name='item_stock']").val($("#update"+id).data("stock"));
		$("input[name='item_purchase_price']").val($("#update"+id).data("purchase"));
		$("input[name='item_sold_price']").val($("#update"+id).data("sold"));
		$("#myModalLabel").text("Edit Data Barang");
		$("#id").val(id);
		$("#submit-form").text("Edit");
		$("#formModal").modal("show");
	}
</script>