<style type="text/css">
	.table thead tr th{
		text-align: center;
	}
	.table thead tr th, .table tbody tr td, .table tfoot tr th{
		vertical-align: middle;
		cursor: pointer;
	}
</style>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Retur Pembelian</h3>
					<div class="pull-right">
						<a type="button" href="<?= base_url();?>panel/returnItem/listReturnPurchasing/create" class="btn btn-primary">Tambah Data</a>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12 table-responsive">
							<table class="table table-striped table-bordered table-hover" id="generalTable">
								<thead>
									<tr>
										<th>#</th>
										<th>Tanggal</th>
										<th>Keterangan</th>
										<th style="width: 100px;">Opsi</th>
									</tr>
								</thead>
								<tbody>
									<?php $start = 0; foreach ($data_array as $data) { ?>
									<tr>
										<td><?= ++$start ?>.</td>
										<td><?= indonesianDate($data["return_create_date"]) ?></td>
										<td><?= $data["return_description"] ?></td>
										<td>
											<a href="javascript:detail(<?= $data['return_id']; ?>)" id="detail<?= $data['return_id']; ?>" data-date="<?= indonesianDate($data["return_create_date"]) ?>" data-description="<?= $data["return_description"] ?>" data-toggle="tooltip" data-placement="top" title="Detail" class="btn btn-sm btn-success" style="width: 50%"><i class="fa fa-info"></i></a>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Detail Retur </h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" action="#">
					<div class="form-group">
						<label class="col-md-3 control-label">Tanggal</label>
						<h5 class="col-md-2" id="return_date">:</h5>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Keterangan</label>
						<h5 class="col-md-3" id="return_description">:</h5>
					</div>
					<div class="form-group">
						<div class="col-md-10 col-md-offset-1">
							<table class="table table-hovered table-bordered table-striped">
								<thead>
									<tr>
										<th>Nomor Barang</th>
										<th>Nama Barang</th>
										<th>Jumlah</th>
									</tr>
								</thead>
								<tbody id="result-data">

								</tbody>
							</table>							
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a data-dismiss="modal" class="btn btn-warning btn-flat">Tutup</a>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		
	});
	function detail(id){
		$.ajax({
			url: '<?= base_url(); ?>panel/returnItem/listReturnPurchasing/detail/'+id,
			beforeSend: function(){
				$("#return_date").text(": "+$("#detail"+id).data("date"));
				$("#return_description").text(": "+$("#detail"+id).data("description"));
				$("#result-data").html('<tr><td colspan="4" class="text-center"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></td></tr>');
				$("#detailModal").modal("show");
			},
			success: function(response){
				$("#result-data").html("");
				var data = $.parseJSON(response);
				$.each(data, function(key,value) {
					$("#result-data").append('<tr><td class="text-center"><b>'+value.number+'</b></td><td>'+value.name+'</td><td class="text-center">'+value.qty+'</td></tr>');
					
				})

			}
		})
	}	
</script>