$(document).ready(function() {
	var url = window.location.pathname
	var activePage = url.split("/")[3];
	var activeMenu = url.split("/")[4];
	var activeParent = url.split("/")[5];
	if(activePage){
		$('.sidebar-menu li').each(function(){
			var currentPage = $(this).data("menu");
			if (activePage == currentPage) {
				$(this).addClass('active');
				$(this).css('border-left','5px solid #3c8dbc');
			}
		})
	}
	if(activeMenu){
		$('.sidebar-menu li ul li').each(function(){
			var currentPage = $(this).data("parent");
			if (activeMenu == currentPage) {
				$(this).addClass('active');
			}
		})
	}
	if(activeParent){
		$('.sidebar-menu li ul li ul li').each(function(){
			var currentPage = $(this).data("parent");
			if (activeParent == currentPage) {
				$(this).addClass('active');
			}
		})
	}
	$.fn.datepicker.defaults.format = "yyyy/mm/dd";
	$(".date").datepicker({ dateFormat: 'dd-mm-yy', autoclose: true, todayBtn: "linked", language: "id"});
	$(".date").datepicker("update", new Date());
	$(".month").datepicker({ format: "mm-yyyy", viewMode: "months", minViewMode: "months",autoclose: true, todayBtn: "linked", language: "id"});

	$('.rupiah').maskMoney({prefix:'Rp ', thousands:'.', decimal:',', precision:0});  
	$('.decimalForm').maskMoney({prefix:'', thousands:'.', decimal:',', precision:0});  
	// $('.timepicker1').timepicker('setTime', '06:00 AM');
	$(".select2").select2();
	$( ".box" ).addClass( "box-primary" );
	$( ".btn" ).addClass( "btn-flat" );
	$( ".select" ).css({"height":"35px","width":"100%"});
	$("#generalTable").DataTable({
		language: {
			processing:     "Memuat data",
			search:         "Cari",
			lengthMenu:    "Tampilkan _MENU_ data",
			info:           "Menampilkan _START_ s/d _END_ dari _TOTAL_ data",
			infoEmpty:      "Data tidak ditemukan",
			infoFiltered:   "",
			loadingRecords: "Memuat Data",
			zeroRecords:    "Data tidak ditemukan",
			emptyTable:     "Data tidak ditemukan",
			paginate: {
				first:      "Pertama",
				previous:   "Sebelumnya",
				next:       "Selanjutnya",
				last:       "Terakhir"
			},
		}
	});
	$("#imgFile").change(function(){
		previewFile(this);
	})
})
function previewFile(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#imgPreview').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}
function goBack(){
	 window.history.back();
}
function toRp(angka){
    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
    var rev2    = '';
    for(var i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += '.';
        }
    }
    return 'Rp ' + rev2.split('').reverse().join('');
}