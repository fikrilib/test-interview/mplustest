/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100125
 Source Host           : localhost:3306
 Source Schema         : koperasi

 Target Server Type    : MariaDB
 Target Server Version : 100125
 File Encoding         : 65001

 Date: 15/09/2018 10:20:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for item
-- ----------------------------
DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `item_number` varchar(255) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_stock` int(11) unsigned DEFAULT NULL,
  `item_purchase_price` decimal(10,0) DEFAULT NULL,
  `item_sold_price` decimal(10,0) DEFAULT NULL,
  `item_create_date` datetime DEFAULT NULL,
  `item_last_update` datetime DEFAULT NULL,
  `item_state` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for item_calculation
-- ----------------------------
DROP TABLE IF EXISTS `item_calculation`;
CREATE TABLE `item_calculation` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for item_type
-- ----------------------------
DROP TABLE IF EXISTS `item_type`;
CREATE TABLE `item_type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for kartu_piutang
-- ----------------------------
DROP TABLE IF EXISTS `kartu_piutang`;
CREATE TABLE `kartu_piutang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) DEFAULT NULL,
  `member_id` int(11) NOT NULL,
  `in` varchar(100) NOT NULL,
  `out` varchar(100) NOT NULL,
  `saldo` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_number` varchar(255) DEFAULT NULL,
  `member_name` varchar(255) DEFAULT NULL,
  `member_address` varchar(255) DEFAULT NULL,
  `member_phone` varchar(255) DEFAULT NULL,
  `member_credit` decimal(10,0) DEFAULT NULL,
  `member_create_date` datetime DEFAULT NULL,
  `member_last_update` datetime DEFAULT NULL,
  `member_state` tinyint(4) DEFAULT NULL COMMENT '1: active, 0: non active',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for purchasing
-- ----------------------------
DROP TABLE IF EXISTS `purchasing`;
CREATE TABLE `purchasing` (
  `purchasing_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `purchasing_number` varchar(255) DEFAULT NULL,
  `purchasing_date` date DEFAULT NULL,
  `purchasing_total` decimal(10,0) DEFAULT NULL,
  `purchasing_discount` decimal(10,0) DEFAULT '0',
  `purchasing_grand_total` decimal(10,0) DEFAULT NULL,
  `purchasing_create_date` datetime DEFAULT NULL,
  `purchasing_state` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`purchasing_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for purchasing_detail
-- ----------------------------
DROP TABLE IF EXISTS `purchasing_detail`;
CREATE TABLE `purchasing_detail` (
  `detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchasing_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `detail_qty` int(11) DEFAULT NULL,
  `detail_price` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for return
-- ----------------------------
DROP TABLE IF EXISTS `return`;
CREATE TABLE `return` (
  `return_id` int(11) NOT NULL AUTO_INCREMENT,
  `return_description` varchar(255) DEFAULT NULL,
  `return_create_date` datetime DEFAULT NULL,
  `return_type` tinyint(1) DEFAULT NULL,
  `return_state` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`return_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for return_detail
-- ----------------------------
DROP TABLE IF EXISTS `return_detail`;
CREATE TABLE `return_detail` (
  `detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `return_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `detail_quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for stok_mutasi
-- ----------------------------
DROP TABLE IF EXISTS `stok_mutasi`;
CREATE TABLE `stok_mutasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `id_order` varchar(11) NOT NULL,
  `stock_qty` int(11) NOT NULL,
  `stock_price` decimal(11,0) NOT NULL,
  `stock_total` decimal(11,0) NOT NULL,
  `balance_stock_qty` int(50) NOT NULL DEFAULT '0',
  `balance_stock_price` decimal(11,0) NOT NULL,
  `balance_stock_total` decimal(11,0) NOT NULL,
  `description` varchar(255) NOT NULL,
  `stock_type` tinyint(1) NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for supplier
-- ----------------------------
DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_number` varchar(255) DEFAULT NULL,
  `supplier_name` varchar(255) DEFAULT NULL,
  `supplier_address` varchar(255) DEFAULT NULL,
  `supplier_phone` varchar(255) DEFAULT NULL,
  `supplier_mail` varchar(255) DEFAULT NULL,
  `supplier_create_date` datetime DEFAULT NULL,
  `supplier_last_update` datetime DEFAULT NULL,
  `supplier_state` tinyint(4) DEFAULT NULL COMMENT '1: active, 0: non active',
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for transaction
-- ----------------------------
DROP TABLE IF EXISTS `transaction`;
CREATE TABLE `transaction` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `transaction_number` varchar(255) DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `transaction_due_date` date DEFAULT NULL,
  `transaction_total` decimal(10,0) DEFAULT NULL,
  `transaction_discount` decimal(10,0) DEFAULT '0',
  `transaction_grand_total` decimal(10,0) DEFAULT NULL,
  `transaction_down_payment` decimal(10,0) DEFAULT NULL,
  `transaction_type` tinyint(1) DEFAULT NULL COMMENT '1: cash, 2: credit',
  `transaction_create_date` datetime DEFAULT NULL,
  `transaction_state` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for transaction_credit_recieve
-- ----------------------------
DROP TABLE IF EXISTS `transaction_credit_recieve`;
CREATE TABLE `transaction_credit_recieve` (
  `recieve_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) DEFAULT NULL,
  `recieve_amount` decimal(10,0) DEFAULT NULL,
  `recieve_type` tinyint(1) DEFAULT NULL COMMENT '1: down payment, 2: regular',
  `recieve_create_date` datetime DEFAULT NULL,
  `recieve_state` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`recieve_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for transaction_detail
-- ----------------------------
DROP TABLE IF EXISTS `transaction_detail`;
CREATE TABLE `transaction_detail` (
  `detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `detail_qty` int(11) DEFAULT NULL,
  `detail_price` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` enum('user','pimpinan','staff','pembelian','penjualan') DEFAULT 'staff',
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 PACK_KEYS=0 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES (1, 'user', 'admin', 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=');
INSERT INTO `user` VALUES (6, 'pimpinan', 'pimpinan', 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
