<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Books extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model(array('m_books'));
		#$this->output->enable_profiler(TRUE);
	}
	
	public function index(){
		$this->data['title']='List Books';
		$this->load->view('books/index',$this->data);
	}
	
	public function get_all(){
		$buku=$this->m_books->get_all()->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($buku, JSON_NUMERIC_CHECK));
	}
	
	public function get_category(){
		$category=$this->m_books->get_categories()->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($category));
	}
	
	public function add_book(){
		$postdata = json_decode(file_get_contents('php://input'), TRUE);
		if (isset($postdata['book'])) {
			$category_code = (isset($postdata['book']['category_code']) ? $postdata['book']['category_code'] : NULL);
			$title = (isset($postdata['book']['title']) ? $postdata['book']['title'] : NULL);
			$author = (isset($postdata['book']['author']) ? $postdata['book']['author'] : NULL);
			$total_pages = (isset($postdata['book']['total_pages']) ? $postdata['book']['total_pages'] : NULL);
			$date_published = (isset($postdata['book']['date_published']) ? $postdata['book']['date_published'] : NULL);
			if ($title == NULL) {
				http_response_code(400);
				$this->output->set_content_type('application/json')->set_output(json_encode(['errors' => ["Title Field is required"]]));
			} else if ($author == NULL){
				http_response_code(400);
				$this->output->set_content_type('application/json')->set_output(json_encode(['errors' => ["Author Field is required"]]));
			} else {
				$data=array(
				'category_code' => $category_code,
				'title' => $title,
				'author' => $author,
				'total_pages' => $total_pages,
				'date_published' => $date_published
				);
				
				if($this->m_books->insert($data)){
					$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Book created successfully</div>');
					redirect('panel/books/index','refresh');
				}
			}
		}
	}
	
	public function delete(){
		$postdata=json_decode(file_get_contents("php://input"));
		if(isset($postdata) && !empty($postdata)){
			$id=(int)$postdata->ID;
			if($this->m_books->delete($id)){
				$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Book delete successfully</div>');
				redirect('panel/books/index','refresh');
			}
		}
	}
	
	public function update(){
		$postdata = json_decode(file_get_contents('php://input'), TRUE);
		if (isset($postdata['book'])) {
			$book_id = (isset($postdata['book']['book_id']) ? $postdata['book']['book_id'] : NULL);
			$category_code = (isset($postdata['book']['category_code']) ? $postdata['book']['category_code'] : NULL);
			$title = (isset($postdata['book']['title']) ? $postdata['book']['title'] : NULL);
			$author = (isset($postdata['book']['author']) ? $postdata['book']['author'] : NULL);
			$total_pages = (isset($postdata['book']['total_pages']) ? $postdata['book']['total_pages'] : NULL);
			$date_published = (isset($postdata['book']['date_published']) ? $postdata['book']['date_published'] : NULL);
			if ($title == NULL) {
				http_response_code(400);
				$this->output->set_content_type('application/json')->set_output(json_encode(['errors' => ["Title Field is required"]]));
			} else if ($author == NULL){
				http_response_code(400);
				$this->output->set_content_type('application/json')->set_output(json_encode(['errors' => ["Author Field is required"]]));
			} else {
				$data=array(
				'book_id' => $book_id,
				'category_code' => $category_code,
				'title' => $title,
				'author' => $author,
				'total_pages' => $total_pages,
				'date_published' => $date_published
				);
				
				if($this->m_books->update($data)){
					$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Book update successfully</div>');
					redirect('panel/books/index','refresh');
				}
			}
		}
	}
}