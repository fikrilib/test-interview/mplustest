<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"/>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<style>
    .submitted .ng-invalid{
        border: 1px solid red;
    }
	</style>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.min.css" rel="stylesheet"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
</head>
<body ng-app="App">
<div class="container" ng-controller="booksCtrl">
	<h3 class="page-header">List Books</h3>
	<!--<button class="btn btn-primary pull-right" data-toggle="modal" data-target="#add_category_modal">Add Category Book</button>-->
	<button class="btn btn-primary pull-right" data-toggle="modal" data-target="#add_books_modal">Add Book</button>
	<hr>
	<?=$this->session->userdata('message_action') ?>
	<table ng-if="books.length > 0" class="table table-hover table-striped">
		<tr>
			<th>No</th>
			<th>Category</th>
			<th>Title</th>
			<th>Author</th>
			<th>Date Published</th>
			<th>Total Pages</th>
			<th>Action</th>
		</tr>
		<tr ng-repeat="x in books">
			<td>{{ $index + 1 }}</td>
			<td>{{ x.category_name }}</td>
			<td>{{ x.title }}</td>
			<td>{{ x.author }}</td>
			<td>{{ x.date_published }}</td>
			<td>{{ x.total_pages }}</td>
			<td>
				<button ng-click="edit($index)" class="btn btn-primary btn-xs">Edit</button>
				<button ng-click="delete(x.book_id)" class="btn btn-danger btn-xs">Delete</button>
			</td>
		</tr>
	</table>
	
<div id="add_books_modal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document"><div class="modal-content">
		<form class="well" name="formTest" ng-class="{'submitted': submitted}" ng-submit="addBook()" >
		<div class="modal-header">
			<h5 class="modal-title">Add / Edit Book</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
		<div class="modal-body">		
			<ul class="alert alert-danger" ng-if="errors.length > 0"><li ng-repeat="error in errors">{{ error }}</li></ul>
			<input type="hidden" ng-model="book.book_id">
			<div class="form-group">
				<label for="category_name">Category Book</label>
				<select class="form-control" ng-model="book.category_code" required>
					<option value="">Choose</option>
					<option ng-repeat="opt in category" value="{{opt.category_code}}">{{opt.category_name}}</option>
				</select>
			</div>
			<div class="form-group">
				<label for="book_title">Title*</label>
				<input type="text" class="form-control" id="booktitle" placeholder="Book Title" ng-model="book.title" required />
			</div>
			<div class="form-group">
				<label for="book_author">Author*</label>
				<input ng-model="book.author" type="text" class="form-control" placeholder="Author" required/>
			</div>
			<div class="form-group">
				<label for="book_date_published">Date Published</label>
				<input ng-model="book.date_published" type="text" class="form-control" id="datepicker" placeholder="Date Published" readonly/>
			</div>
			<script>
				$('#datepicker').datepicker({
					format: 'yyyy-mm-dd'
				});
			</script>
			<div class="form-group">
				<label for="book_total_pages">Total Pages</label>
				<input type="number" class="form-control" placeholder="Total Pages" ng-model="book.total_pages"/>
			</div>
		</div>
		<div class="modal-footer">
		<button type="submit" class="btn btn-primary" ng-click="submitted= true;" ng-disabled="disabledAdd">Simpan</button>
		<button type="button" class="btn btn-info" ng-click="updateBook()" ng-disabled="disabledUpdate">Ubah</button>
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
		</div>
		</form>
	</div>
	</div>
</div>


</div>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.2/angular.min.js"></script>
<script type="text/javascript">
var app = angular.module('App', []);
app.controller('booksCtrl',function($scope,$http){
	$scope.errors = [];
	$scope.books = []; //List Produk
	$scope.book = {}; //Object
	$scope.category;
	$scope.disabledAdd=false;
	$scope.disabledUpdate=true;
	
	$scope.getCategory = function() {
		$http({
			method: 'GET',
			url: '<?php echo site_url('books/get_category/')?>'
		}).then(function success(e) {
			$scope.category = e.data;
		}, function error(e) {
			console.log(e.data,e.error);
		});
	};
	$scope.getCategory();
	
	$scope.getbooks = function() {
		$http({
			method: 'GET',
			url: '<?php echo site_url('books/get_all/')?>'
		}).then(function succes(e) {
			$scope.books = e.data;
		}, function error(e) {
			console.log(e.data,e.error);
		});
	};
	
	$scope.addBook=function() {
		$http({
			method: 'POST',
			url:  '<?php echo site_url('books/add_book/')?>',
			data :{book: $scope.book}
		}).then(function success(e) {
			$scope.errors = [];
			$scope.books.push($scope.book);
			var modal_element = angular.element('#add_books_modal');
			modal_element.modal('hide');
			window.location.reload();
		}, function error(e) {
			$scope.errors = e.data.errors;
		});
	}
	
	$scope.delete=function(id_prod) {
		var conf = confirm("Are you sure delete this book ?");
		if (conf == true) {
			$http({
				method: 'POST',
				url:  '<?php echo site_url('books/delete/')?>',
				data: { ID : id_prod }
			}).then(function success(e) {
				window.location.reload();
			}, function error(e) {
				$scope.errors = e.data.errors;
				console.log(e.data,e.errors);
			});
		}
	}
	
	$scope.edit = function (index) {
		$scope.book = $scope.books[index];
		$scope.disabledAdd=true;
		$scope.disabledUpdate=false;
		var modal_element = angular.element('#add_books_modal');
		modal_element.modal('show');
	};
	
	$scope.updateBook=function() {
		$http({
			method: 'POST',
			url:  '<?php echo site_url('books/update/')?>',
			data :{book: $scope.book}
		}).then(function success(e) {
			$scope.errors = [];
			var modal_element = angular.element('#add_books_modal');
			modal_element.modal('hide');
			window.location.reload();
		}, function error(e) {
			$scope.errors = e.data.errors;
		});
	}
	
	$scope.getbooks();
});
</script>
</body>
</html>